# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "Popcorn",
			"color": "#013781",
			"icon": "fa fa-file-contract",
			"type": "module",
			"label": _("Popcorn")
		}
	]
