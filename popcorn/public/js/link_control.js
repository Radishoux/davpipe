frappe.provide("frappe.ui")
frappe.provide("popcorn.ui")

popcorn.ui.ControlLink = frappe.ui.form.ControlLink.extend({
	make_input: function() {
		var me = this;
		// line-height: 1 is for Mozilla 51, shows extra padding otherwise
		$('<div class="link-field ui-front" style="position: relative; line-height: 1;">\
			<input type="text" class="input-with-feedback form-control">\
			<span class="link-btn">\
				<a class="btn-open no-decoration" title="' + __("Open Link") + '">\
					<i class="octicon octicon-arrow-right"></i></a>\
			</span>\
		</div>').prependTo(this.input_area);
		this.$input_area = $(this.input_area);
		this.$input = this.$input_area.find('input');
		this.$link = this.$input_area.find('.link-btn');
		this.$link_open = this.$link.find('.btn-open');
		this.set_input_attributes();
		this.$input.on("focus", function() {
			setTimeout(function() {
				if(me.get_input_value() && me.get_options()) {
					let doctype = me.get_options();
					let name = me.get_input_value();
					me.$link.toggle(true);
					me.$link_open.attr('href', frappe.utils.get_form_link(doctype, name));
				}

				if(!me.$input.val()) {
					me.$input.val("").trigger("input");
				}
			}, 500);
		});
		this.$input.on("blur", function() {
			// if this disappears immediately, the user's click
			// does not register, hence timeout
			setTimeout(function() {
				me.$link.toggle(false);
			}, 500);
		});
		this.$input.attr('data-target', this.df.options);
		this.input = this.$input.get(0);
		this.has_input = true;
		this.translate_values = true;
		this.setup_buttons();
		this.setup_awesomeplete();
		this.bind_change_event();
	},
	format_for_input: function (value) {
		if (value) {
			return __(value)
		} else {
			return this._super(value)
		}
	},

	set_formatted_input: function(value) {
		if (this.$input) {
			this.$input.val(this.format_for_input(value))
			this.$input.data("value", value);
		}
	},

	get_input_value: function () {
		return this.$input ? this.$input.data("value") : undefined;
	},

	get_value: function () {
		return this.$input ? (this.$input.data("value") || this.$input.val()) : (this.value || undefined);
	}
});

$(document).ready(() => {
	frappe.ui.form.ControlLink = popcorn.ui.ControlLink
})