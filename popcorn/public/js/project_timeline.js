frappe.provide("popcorn.ui");

import BaseTimeline from "./base_timeline";

const task_status_color = {
	"Not Started": "light-gray",
	Active: "blue",
	Completed: "green",
	Suspended: "yellow",
	Cancelled: "red",
	Late: "orange",
};

popcorn.ui.project_timeline = class ProjectTimeline extends BaseTimeline {
	constructor(opts) {
		super(opts);
		Object.assign(this, opts);
	}

	prepare_timeline_contents() {
		this.timeline_items.push(...this.get_project_creation_contents());
		this.timeline_items.push(...this.get_tasks_timeline_contents());
	}

	get_project_creation_contents() {
		const content = `
			<div class="row">
				<div class="text-muted col-12 col-xl-6">${__(
					"Project Created"
				)}</div>
				<div class="col-12 col-xl-6">${frappe.datetime.obj_to_user(
					this.frm.doc.creation
				)}</div><div class="col-12">${frappe.avatar(
			this.frm.doc.owner,
			"avatar-xs"
		)}</div>
			</div>
		`;
		return [
			{
				timeline_badge: this.get_timeline_badge("archive", "green"),
				creation: this.frm.doc.creation,
				content: content,
				hide_timestamp: true,
			},
		];
	}

	get_tasks_timeline_contents() {
		return (this.tasks || []).map((t) => {
			return {
				timeline_badge: this.get_timeline_badge(
					t.step.toLowerCase(),
					task_status_color[t.status]
				),
				creation: t.creation,
				content: `${this.get_task_preview(t)}`,
				hide_timestamp: true,
			};
		});
	}

	get_task_preview(task) {
		const avatar = task.completed_by
			? `<div class="col-12">${frappe.avatar(
					task.completed_by,
					"avatar-xs"
				)}</div>`
			: "";
		return `
			<div class="row">
				<div class="text-muted col-12 col-xl-6">${__(
					task.step
				)}</div>
				<div class="col-12 col-xl-6">${frappe.datetime.obj_to_user(
					task.status == "Completed" ? task.end_date : task.expected_end_date
				)}</div>
				${avatar}
			</div>
		`;
	}

	get_timeline_badge(icon, color) {
		return `
			<div class="timeline-badge timeline-${color}">
				${popcorn.utils.icon(icon, "sm")}
			</div>
		`;
	}
};
