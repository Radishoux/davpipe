$(document).ready(() => {
	Object.assign(frappe.breadcrumbs, {
		set_list_breadcrumb(breadcrumbs) {
			const doctype = breadcrumbs.doctype;
			if ((doctype==="User" && !frappe.user.has_role('System Manager'))
				|| frappe.get_doc('DocType', doctype).issingle) {
				// no user listview for non-system managers and single doctypes
			} else {
				let route;
				const doctype_route = frappe.router.slug(frappe.router.doctype_layout || doctype);
				if (frappe.boot.treeviews.indexOf(doctype) !== -1) {
					let view = frappe.model.user_settings[doctype].last_view || 'Tree';
					route = `${doctype_route}/view/${view}`;
				} else {
					route = doctype_route;
				}
				$(`<li><a href="/app/${route}">${__(doctype)}</a></li>`)
					.appendTo(this.$breadcrumbs)
			}
		},
		set_form_breadcrumb(breadcrumbs, view) {
			const doctype = breadcrumbs.doctype;
			const docname = frappe.get_route()[2];
			let form_route = `/app/${frappe.router.slug(doctype)}/${docname}`;
			$(`<li><a href="${form_route}">${__(docname)}</a></li>`)
				.appendTo(this.$breadcrumbs);

			if (view === "form") {
				let last_crumb = this.$breadcrumbs.find('li').last();
				last_crumb.addClass('disabled');
				last_crumb.css("cursor", "copy");
				last_crumb.click((event) => {
					event.stopImmediatePropagation();
					frappe.utils.copy_to_clipboard(last_crumb.text());
				});
			}

		},
	})
});