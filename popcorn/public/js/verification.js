import Vue from 'vue/dist/vue.js';

if (!window.Vue) {
	Vue.prototype.__ = window.__;
	Vue.prototype.frappe = window.frappe;
	window.Vue = Vue;
}

frappe.provide("popcorn.ui")

popcorn.ui.verification = class Verification {
	constructor(opts) {
		Object.assign(this, opts)
		this.result = []
		this.$audit_wrapper = $(this.audit_wrapper)
		this.$file_uploader = $(this.upload_wrapper).find(".file-uploader")
		this.query_params = frappe.utils.get_query_params()
		this.make()
	}

	make() {
		if (this.query_params && this.query_params.hash) {
			this.get_verification_from_hash()
		} else {
			this.show_uploader()
		}
	}

	get_verification_from_hash() {
		frappe.call("popcorn.utils.audit.verify_hash", {hash: this.query_params.hash})
		.then(r => {
			if (r && r.message) {
				this.result = r.message || [];
				this.process_result()
			}
		})
	}

	show_uploader() {
		const me = this;
		this.file_uploader = new popcorn.ui.FileUploader({
			wrapper: this.$file_uploader,
			disable_file_browser: 1,
			restrictions: {
				allowed_file_types: ["application/pdf"],
			},
			allow_multiple: false,
			show_upload_button: false,
			on_add(files) {
				files.map((file, i) => {
					this.upload_file(file, i, "/api/method/popcorn.upload.upload_file_for_verification")
				})
			},
			on_success(f, result) {
				console.log(result)
				if (result && result.message) {
					me.result = result.message || [];
					me.process_result()
				}
			},
			on_remove() {
				me.reset_result()
			}
		});
		$(this.upload_wrapper).removeClass("hide")
	}

	process_result() {
		if (this.result.length && this.result[0].file_name) {
			const text = this.result.length === 1 ? __("We found a document corresponding to your file") :
				__("We found {0} documents corresponding to your file", [this.result.length])
			this.$audit_wrapper.find(".result").append(
				this.result_section("solid-success", text)
			)

			this.append_details()
		} else {
			this.$audit_wrapper.find(".result").append(
				this.result_section("solid-error", __("We could not find a corresponding document for your file"))
			)
		}
	}

	result_section(icon, text) {
		this.$audit_wrapper.removeClass("hide")
		return `<div>
				<div class="flex align-items-center justify-content-center">
					<div>${frappe.utils.icon(icon, 'xl')}</div>
					<div><h3 class="h3">${text}</h3></div>
				</div>
				<div>
					<div class="text-center">
						<div class="text-muted">${__("The document digital print (hash) is:")}</div>
						<div class="h5">${this.result[0].hash}<div>
					</div>
				<div>
			</div>`
	}

	reset_result() {
		this.$audit_wrapper.find(".result").empty()
		this.$audit_wrapper.find(".details").empty()
		this.$audit_wrapper.addClass("hide")
	}

	append_details() {
		const has_status = this.result.reduce((prev, curr) => {
			return curr.status ? prev + 1 : prev
		}, 0)

		const details = has_status > 0 ? this.result.map((data, i) => {
			return this.details_section(data, i)
		}) : $(`<div class="my-8 px-6">
				<div class="head-title text-center">${__("Blockchain verification information are not available yet. Please try again later.")}</div>
			</div>`)

		this.$audit_wrapper.find(".details").append(details)
	}

	details_section(data, index) {
		const title = this.result.length > 1 ? `<div class="head-title">${data.file_name ? data.file_name : __("Document {0}", [index + 1])}</div>` : ""
		const section = $(`
			<div class="my-8 px-6">
				${title}
				${this.status_section(data) || ""}
				${this.blockchain_section(data) || ""}
			</div>
			`)
		section.append(this.proof_section(data)).append('<div class="dropdown-divider"></div>')
		return section
	}

	status_section(data) {
		if (!data.status) return

		const status_map = {
			"Injected": "injection_datetime",
			"Validated": "validation_datetime",
			"Rejected": null,
			"Inserted": "insertion_datetime",
			"Retried": null
		}

		const status = __("This document has been {0} in the Tezos Blockchain on {1}.", [data.status.toLowerCase(), data[status_map[data.status]]])
		const validator = data.signature_verification ? __("This document has been signed by Davron Digital with the public key {0} and registered on the smart contract {1}.", [data.proof.public_key, data.proof.contract_address])
			: __("This document has been signed by the public key {0} and registered on the smart contract {1}.", [data.proof.public_key, data.proof.contract_address])

		const validator_section = data.proof.public_key ? `
			<div class="col col-md-12">
				<div class="mb-4">
					<div class="text-muted">${__("Blockchain validator")}</div>
					<div class="control-label">${validator}</div>
				</div>
			</div>
		` : ""

		return status_map[data.status] ? `
			<div class="row">
				<div class="col col-md-12">
					<div class="mb-4">
						<div class="text-muted">${__("Blockchain validation status")}</div>
						<div class="control-label">${status}</div>
					</div>
				</div>
				${validator_section}
			</div>
		` : ""
	}

	blockchain_section(data) {
		return (data.block_hash || data.transaction_hash) ?
			`<div class="row">
				<div class="col col-md-6">
					<div class="mb-4">
						<div class="text-muted">${__("Blockchain Block")}</div>
						<div class="control-label"><a href="${data.block_url}" target="_blank">${data.block_hash || ""}</a></div>
					</div>
				</div>
				<div class="col col-md-6">
					<div class="mb-4">
						<div class="text-muted">${__("Blockchain Transaction")}</div>
						<div class="control-label"><a href="${data.transaction_url}" target="_blank">${data.transaction_hash || ""}</a></div>
					</div>
				</div>
			</div>
		` : ""
	}

	proof_section(data) {
		if (!data.proof) {
			return ""
		}

		const section = (Object.keys(data.proof).length && data.proof.urls) ?
			$(`<div class="row">
				<div class="col col-md-6">
					<div class="mb-4">
						<div class="text-muted">${__("Merkel Tree Root Hash")}</div>
						<div class="control-label">
							${data.merkel_root || ""}
						</div>
					</div>
				</div>
				<div class="col col-md-12">
					<li class="list-group-item">
						<div class="section-preview">
							<span class="download-button"><i class="fa fa-download"></i></span>
							<span class="ml-1">${__("Full proof")}</span>
							<span class="float-right flex">
								<div class="expand-button"><span>${__("Expand")} ${frappe.utils.icon("down", 'xs')}</span></div>
							</span>
						</div>
						<div class="section-details mt-4" style="display: None;">
							<div class="row">
								<div class="col col-md-12">
									<div class="mb-4">
										<div class="control-label">
											<pre>${JSON.stringify(data.proof, null, '\t')}</pre>
										</div>
									</div>
								</div>
							</div>
						</div>
					</li>
				</div>
			</div>
		`) : $("")

		section.find(".expand-button").on("click", () => {
			const meta = section.find(".section-details").toggle()
			section.find(".expand-button").html($(meta).is(":visible") ?
				`<span>${__("Collapse")} ${frappe.utils.icon("up-line", 'xs')}</span>` :
				`<span>${__("Expand")} ${frappe.utils.icon("down", 'xs')}</span>`
			)
		})

		if (data.proof) {
			section.find(".download-button").on("click", () => {
				downloadify(JSON.stringify(data.proof), data.file_name)
			})
		}

		return section
	}
}

const downloadify = (data, title) => {
	var filename = title + ".json";
	var a = document.createElement('a');

	var blob_object = new Blob([data], { type: 'text/csv;charset=UTF-8' });
	a.href = URL.createObjectURL(blob_object);
	a.download = filename;

	document.body.appendChild(a);
	a.click();

	document.body.removeChild(a);
};