frappe.provide("popcorn.ui")

popcorn.ui.FormSidebar = class FormSidebar extends frappe.ui.form.Sidebar {
	constructor(opts) {
		super(opts)
	}

	make() {
		this.page.sidebar.addClass("col-lg-4").removeClass("col-lg-2")
		this.sidebar = $('<div class="form-sidebar overlay-sidebar hidden-xs hidden-sm"></div>')
			.appendTo(this.page.sidebar.empty());

		this.make_recap_card()
		this.make_steps_card()

		this.refresh();
	}

	refresh() {
		//
	}

	make_recap_card() {
		this.sidebar.append(`
			<div class="frappe-card">
				<div class="row">
					<div class="col col-md-6">
						<div>
							<div class="text-muted">${__("Country of destination")}</div>
							<div class="control-label">${__(this.frm.doc.country_of_destination) || ""}</div>
						</div>
					</div>
					<div class="col col-md-6">
						<div>
							<div class="text-muted">${__("Type of legalization")}</div>
							<div class="control-label">${__(this.frm.doc.type_of_legalization) || ""}</div>
						</div>
					</div>
				</div>
			</div>
		`)
	}

	make_steps_card() {
		const $timeline_section = this.sidebar.append(`
			<div class="frappe-card mt-3">
				<div class="timeline-section"></div>
			</div>
		`)

		this.timeline = new popcorn.ui.project_timeline({
			parent: $timeline_section.find(".timeline-section"),
			frm: this.frm,
			tasks: this.tasks
		})
		this.timeline.refresh()
	}
}