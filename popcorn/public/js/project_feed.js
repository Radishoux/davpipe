frappe.provide("popcorn.ui")

const status_map = {
	"Completed": "green",
	"Active": "blue",
	"Cancelled": "red",
	"Suspended": "yellow",
	"Late": "orange",
	"Draft": "light-gray",
	"Archived": "gray"
}

popcorn.ui.ProjectFeed = class ProjectFeed {
	constructor(opts) {
		Object.assign(this, opts)
		this.parent.empty();
		this.task_cards = []
		this.customer = frappe.boot.user_data.user_type == "Customer"
		this.make()

		frappe.realtime.on("refresh_project", () => {
			this.parent.empty();
			this.make()
		})
	}

	make() {
		this.recap_card = new ProjectCard({
			parent: this,
			title: __("Project Recap"),
			subtitle: __(this.doc.type_of_document),
			docname: this.doc.name,
			body: this.get_recap_card_body()
		})

		!this.recap_only&&new FileSection({
			card: this.recap_card.$card.find(".project-card-body"),
			title: __("Signed documents"),
			doctype: "Project",
			docname: this.doc.name,
			file_status: "Original"
		})

		this.show_tasks().then(() => {
			if (this.doc.status && ["Completed", "Archived"].includes(this.doc.status)) {
				new BlockchainRecap({
					parent: this,
					title: __("Blockchain Validation"),
					subtitle: __("Tezos Blockchain Anchor"),
					project: this.doc.name
				});

				new CertificateSection({
					parent: this,
					title: __("Signature Certificate"),
					subtitle: __("Download a file certificate for your finalized document"),
					tasks: this.tasks
				})

				this.frm.page.set_secondary_action(__("See Audit Log"), () => {
					frappe.set_route("audit", this.doc.name)
				})

			}
		})

		!this.recap_only&&new FileSection({
			card: this.recap_card.$card.find(".project-card-body"),
			title: __("Additional documents"),
			doctype: "Project",
			docname: this.doc.name,
			file_status: "Additional"
		})
	}

	show_tasks() {
		return new Promise((resolve, reject) => {
			frappe.model.with_doctype("Task", () => {
				const meta = frappe.get_meta("Task")
				this.tasks.forEach((t, index, array) => {
	
					if (t.display) {
						const display_action = (this.customer && t.customer_action) || (!this.customer && t.supplier_action)
						const task_card = new ProjectCard({
							parent: this,
							title: __(t.step),
							subtitle: t.supplier,
							status: t.status,
							action_label: display_action && __(t.action_name),
							action: display_action && this.task_action(t),
							status_color: status_map[t.status],
							body: this.get_task_body(t, meta)
						})
	
						new FileSection({
							card: task_card.$card.find(".project-card-body"),
							title: __("Uploaded documents"),
							doctype: "Task",
							docname: t.name,
							file_status: "Supplied"
						})
	
						new FileSection({
							card: task_card.$card.find(".project-card-body"),
							title: __("Additional documents"),
							doctype: "Task",
							docname: t.name,
							file_status: "Optional"
						})
		
						this.task_cards.push(task_card)
						if (index === array.length -1) {
							resolve();
						}
					}
				})
			});
		})
	}

	task_action(task) {
		return function() {
			new popcorn.task_action({task: task})
		}
	}

	get_recap_card_body() {
		const doc = this.doc
		const left_fields = ["country_of_destination", "issuing_country", "type_of_legalization", "type_of_notarization", "type_of_document", "translate_doc", "certification_by_ccfa"].map(f => {
			return doc[f] ? `
				<div class="mb-4">
					<div class="text-muted">${__(this.fields_dict[f].df.label) || ""}</div>
					<div class="control-label">${typeof doc[f] === "number" ? __("Yes") : __(doc[f]) || ""}</div>
				</div>
			` : ""
		}).join("")

		const end_date = ["Completed", "Archived"].includes(doc.status) ? doc.end_date : doc.expected_end_date
		const dates = `
			<div class="mb-4">
				<div class="text-muted">${__("Dates")}</div>
				<div class="control-label">${frappe.datetime.obj_to_user(doc.start_date)} - ${frappe.datetime.obj_to_user(end_date)}</div>
			</div>
		`
		const delivery = doc.send_documents ? `
			<div class="mb-4">
				<div class="text-muted">${__("Delivery")}</div>
				<div class="control-label">${frappe.boot.user_data.delivery_address_display}</div>
			</div>
		` : ""

		const specific_comments = doc.specific_comments ? `
			<div class="mb-4">
				<div class="text-muted">${__("Specific comments")}</div>
				<div class="control-label">${doc.specific_comments}</div>
			</div>
		` : ""

		return `
			<div class="row">
				<div class="col col-md-6">
					${left_fields}
				</div>
				<div class="col col-md-6">
					${dates}
					${delivery}
				</div>
				<div class="col col-md-12">
					${specific_comments}
				</div>
			</div>
		`
	}

	get_task_body(task, meta) {
		const left_fields = [task.status == "Completed" ? "end_date" : "expected_end_date"].map(f => {
			return task[f] ? `
				<div class="mb-4">
					<div class="text-muted">${__(meta.fields.filter(m => m.fieldname == f)[0].label) || ""}</div>
					<div class="control-label">${frappe.datetime.obj_to_user(task[f]) || ""}</div>
				</div>
			` : ""
		}).join("")

		return `
			<div class="row">
				<div class="col col-md-6">
					${left_fields}
				</div>
			</div>
		`
	}
}

class ProjectCard {
	constructor(opts) {
		Object.assign(this, opts)
		this.make()
	}

	make() {
		this.build_card()
	}

	build_card() {
		const status = this.status ? `<span class="indicator-pill ${this.status_color || "gray"} ml-2">${__(this.status)}</span>` : ""
		const action_button = this.action ? $(`<button class="btn btn-sm btn-primary task-action">${this.action_label || __("Action")}</button>`) : ""

		const head = !this.parent.recap_only ? `
			<div class="project-card-head">
				<div class="flex">
					<div class="project-card-title flex">${this.title || ""}${status}</div>
					<div class="ml-auto action-button"></div>
				</div>
				<div class="project-card-subtitle text-muted ${!this.action ? 'mt-1' : ''}">${this.subtitle || ""}</div>
			</div>
		` : `
			<div class="project-card-head">
				<div class="project-card-subtitle text-muted mt-1">${this.subtitle || ""}</div>
				<div class="signed-file-preview"></div>
			</div>
		`

		this.$card = $(`
			<div class="project-feed-card ${ !this.parent.recap_only ? 'frappe-card' : ''}">
				${head}

				<div class="project-card-body mt-2">
					${this.body || ""}
				</div>
			</div>
		`)

		this.parent.recap_only&&new FileSection({
			card: this.$card.find(".signed-file-preview"),
			title: "",
			doctype: "Project",
			docname: this.docname,
			file_status: "Original",
			read_only: true
		})

		this.action&&this.$card.find(".action-button").html(action_button)

		this.action && action_button.on('click', () => {
			this.action()
		})

		this.parent.parent.prepend(this.$card)
	}
}

class FileSection {
	constructor(opts) {
		Object.assign(this, opts)
		this.$wrapper = $(`<div></div>`).appendTo(this.card)
		this.get_data().then(r => {
			this.files = r
			this.files.length&&this.make()
		})
	}

	get_data() {
		return frappe.xcall("popcorn.utils.file.get_files", {
			doctype: this.doctype,
			docname: this.docname,
			file_status: this.file_status
		})
	}

	make() {
		const file_list = this.files.map(f => {
			const signature_metadata = f.signature_metadata&&JSON.parse(f.signature_metadata)
			const latest_signature = signature_metadata&&signature_metadata.length ? signature_metadata.reduce((prev, curr) => {
				return (prev.signDate > curr.signDate) ? prev : curr
			}) : {}

			const issuer = latest_signature.certificateInfo&&latest_signature.certificateInfo.issuerOIDs.O

			const validity = latest_signature.certificateInfo && moment().isAfter(latest_signature.certificateInfo.notValidBefore) && moment().isBefore(latest_signature.certificateInfo.notValidAfter)

			const download_btn = this.read_only ? '' : `<span class="download-button"><i class="fa fa-download"></i></span>`

			const certificate = ["Primary", "Secondary"].includes(f.file_status) ? `
				<div class="mb-4 file-certificate">
					<div class="control-label"><i class="fa fa-hand-o-right"></i> <span>${__("Get a certificate for this file")}</span></div>
				</div>
			` : ""

			const $file_section = $(`
				<li class="list-group-item">
					<div class="file-section-preview">
						${download_btn}
						<span class="ml-1">${f.file_name}</span>
						<span class="float-right flex">
							<div class="expand-button"><span>${__("Expand")} ${frappe.utils.icon("down", 'xs')}</span></div>
						</span>
					</div>
					<div class="file-metadata mt-4" style="display: None;">
						<div class="row">
							<div class="col col-md-12">
								<div class="mb-4">
									<div class="text-muted">${__("Signature")}</div>
									<div class="control-label">${Object.keys(latest_signature).length ? __("Yes") : __("No") || ""}</div>
								</div>
								<div class="mb-4">
									<div class="text-muted">${__("Certificate")}</div>
									<div class="control-label">${issuer || ""}</div>
								</div>
								<div class="mb-4">
									<div class="text-muted">${__("Valid")}</div>
									<div class="control-label">${validity ? __("Yes") : __("No") || ""}</div>
								</div>
								<div class="mb-4">
									<div class="text-muted">${__("File Hash") || ""}</div>
									<div class="control-label">${f.sha256 || ""}</div>
								</div>
								${certificate}
							</div>
						</div>
					</div>
				</li>
			`)

			$file_section.find(".expand-button").on("click", () => {
				const meta = $file_section.find(".file-metadata").toggle()
				$file_section.find(".expand-button").html($(meta).is(":visible") ?
					`<span>${__("Collapse")} ${frappe.utils.icon("up-line", 'xs')}</span>` :
					`<span>${__("Expand")} ${frappe.utils.icon("down", 'xs')}</span>`
				)
			})

			$file_section.find(".download-button").on("click", () => {
				this.download_file(f.name)
			})

			$file_section.find(".file-certificate").on("click", () => {
				this.download_certificate(f.sha256)
			})

			return $file_section
		})

		const $files_display = $(`
			<div class="row">
				<div class="col col-md-12">
					<div class="mb-4">
						<div class="text-muted">${this.title}</div>
						<div class="mt-2">
							<ul class="list-group"></ul>
						</div>
					</div>
				</div>
			</div>`)

		$files_display.find("ul").html(file_list)

		this.$wrapper.html($files_display)
	}

	download_file(filename) {
		var w = window.open(
			frappe.urllib.get_full_url(
				"/api/method/popcorn.utils.file.download_file?"
				+"filename="+encodeURIComponent(filename)));
		if(!w) {
			frappe.msgprint(__("Please enable pop-ups")); return;
		}
	}

	download_certificate(hash) {
		var w = window.open(
			frappe.urllib.get_full_url(
				"/api/method/popcorn.utils.file.download_certificate?"
				+"hash="+encodeURIComponent(hash)));
		if(!w) {
			frappe.msgprint(__("Please enable pop-ups")); return;
		}
	}
}

class BlockchainRecap {
	constructor(opts) {
		Object.assign(this, opts)
		this.hash = []
		this.$wrapper = $(`<div></div>`).prependTo(this.parent.parent)
		let link = ""

		this.get_hash().then(r => {
			const result = r.slice(r.length - 1, r.length)
			this.hash_body = result.map(hash => {
				const transaction = hash.transaction_hash ? `
					<div class="col col-md-6">
						<div class="mb-4">
							<div class="text-muted">${__("Transaction")}</div>
							<div class="control-label transaction-hash">${hash.transaction_hash || ""}</div>
						</div>
						<div class="mb-4">
							<div class="text-muted">${__("Block")}</div>
							<div class="control-label block-hash">${hash.block_hash || ""}</div>
						</div>
					</div>` : ""

				link = hash.transaction_hash ? `
						<a class="btn btn-sm btn-secondary" target="_blank" href="${frappe.boot.tezos_transaction_url}${hash.transaction_hash}">${__("See the transaction on Tezos")}</a>
				` : ""

				return `
					<div class="row">
						<div class="col col-md-6">
							<div class="mb-4">
								<div class="text-muted">${__("Status")}</div>
								<div class="control-label">${__(hash.status || __("Not inserted"))}</div>
							</div>
						</div>
						${transaction}
					</div>`
			}).join("")

			const head = `
				<div class="project-card-head">
					<div class="flex">
						<div class="project-card-title flex">${this.title || ""}</div>
						<div class="ml-auto action-button">${link}</div>
					</div>
					<div class="project-card-subtitle text-muted">${this.subtitle || ""}</div>
				</div>
			`
			this.build_card(head)
		})
	}

	get_hash() {
		return frappe.xcall("popcorn.utils.audit.get_audit_logs", {
			project: this.project,
		})
	}

	build_card(head) {
		this.$card = $(`
			<div class="project-feed-card frappe-card">
				${head}

				<div class="project-card-body mt-2">
					<div class="row">
						<div class="col col-md-12">
							${this.hash_body}
						</div>
					</div>
				</div>
			</div>
		`)

		this.$wrapper.html(this.$card)
	}
}

class CertificateSection {
	constructor(opts) {
		Object.assign(this, opts)
		this.$wrapper = $(`<div></div>`).prependTo(this.parent.parent)
		this.last_task = this.tasks.filter(t => t.step_number == this.tasks.length - 1).map(t => t.task)[0]
		this.get_data().then(r => {
			if (r && r.length) {
				const secondary = r.filter(f => f.file_status == "Secondary")
				const primary = r.filter(f => f.file_status == "Primary")
				this.file = secondary.length ? secondary[0] : primary[0]
				this.build_card()
			}
		})
	}

	get_data() {
		return frappe.xcall("popcorn.utils.file.get_files", {
			doctype: "Task",
			docname: this.last_task,
			file_status: "Supplied"
		})
	}

	build_card() {
		this.$card = $(`
			<div class="project-feed-card frappe-card">
				<div class="project-card-head">
					<div class="flex">
						<div class="project-card-title flex">${this.title || ""}</div>
						<div class="ml-auto download-button">
							<button class="btn btn-sm btn-primary">${__("Download")}</button>
						</div>
					</div>
					<div class="project-card-subtitle text-muted">${this.subtitle || ""}</div>
				</div>
			</div>
		`)

		this.$card.find(".download-button").on("click", () => {
			this.download_certificate(this.file.sha256, this.file.name)
		})

		this.$wrapper.html(this.$card)
	}

	download_certificate(hash, filename) {
		var w = window.open(
			frappe.urllib.get_full_url(
				"/api/method/popcorn.utils.file.download_file_and_certificate?"
				+"hash="+encodeURIComponent(hash)+"&filename="+encodeURIComponent(filename)));
		if(!w) {
			frappe.msgprint(__("Please enable pop-ups")); return;
		}
	}
}