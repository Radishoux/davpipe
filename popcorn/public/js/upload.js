// Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and Contributors
// MIT License. See license.txt

import FileUploader from './file_uploader';

frappe.provide('popcorn.ui');
popcorn.ui.FileUploader = FileUploader;