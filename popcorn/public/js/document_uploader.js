frappe.provide("popcorn")
frappe.provide("popcorn.ui")
frappe.provide("popcorn.project_updates")
frappe.utils.make_event_emitter(popcorn.project_updates);

popcorn.documents_list = class DocumentsList {
	constructor(opts) {
		Object.assign(this, opts)
		this.documents_field = this.slide.get_field(this.field)
		this.files = []
		this.file_uploader = {}
		this.files_uploaded = true
		this.upload_in_progress = false
		popcorn.project_updates.on("refresh_list", data => {
			this.refresh_list(data.removed_file)
		})

		this.make()
	}

	make() {
		const label = $(`<label class="control-label" style="padding-right: 0px;">${this.label || ""}</label>`)
		this.file_uploader_wrapper = $("<div></div>")
		this.documents_field.$wrapper.empty()
		this.documents_field.$wrapper.append(label).append(this.file_uploader_wrapper)
		this.get_data().then(() => {
			this.show()
		})
	}

	get_data() {
		return frappe.xcall(this.method, {project: this.project.name})
			.then(r => {
				this.files = r.map(f => {
					return Object.assign(f, {
						type: "application/pdf",
						docname: f.name,
						name: f.file_name,
						progress: 1,
						total: 1
					})
				})

				if (!this.files.length) {
					this.files_uploaded = false
				}
			})
	}

	refresh_list(removed_file) {
		this.files = this.files.filter(f => f.name != removed_file)
		this.show()
	}

	show() {
		const me = this;
		this.file_uploader = new popcorn.ui.FileUploader({
			wrapper: this.file_uploader_wrapper,
			doctype: this.project.doctype,
			docname: this.project.name,
			status: this.status || "Optional",
			folder: `Home/${this.project.name}`,
			disable_file_browser: 1,
			show_upload_button: false,
			files: this.files,
			on_success(file_doc) {
				me.files_uploaded = true
				this.upload_in_progress = false
			}
		});
	}

	upload_files() {
		if (!this.upload_in_progress&&!this.files_uploaded) {
			this.upload_in_progress = true
			return this.file_uploader.upload_files()
		} else {
			return true
		}
	}

	are_file_uploaded() {
		return this.files_uploaded
	}
}
