frappe.provide("popcorn")
frappe.provide("popcorn.ui")
frappe.provide("popcorn.task_action")
frappe.provide("popcorn.task_updates")
frappe.utils.make_event_emitter(popcorn.task_updates);

popcorn.task_action = class TaskAction {
	constructor(opts) {
		Object.assign(this, opts)
		this.customer = frappe.boot.user_data.user_type == "Customer"
		this.task_action_detail = this.customer ? CustomerTaskDetails : SupplierTaskDetails
		this.make()
	}

	make() {
		this.get_data().then(() => {
			this.show()
		})
	}

	get_data() {
		return frappe.xcall("popcorn.popcorn.doctype.task.task.get_task_files", {task: this.task.name})
		.then(r => {
			this.files = r;
		})
	}

	show() {
		this.dialog = new frappe.ui.Dialog({
			title: this.task.dialog_title,
			size: "large",
			fields: [
				{
					fieldname: "action",
					fieldtype: "HTML"
				}
			]
		})

		new this.task_action_detail({
			dialog: this.dialog,
			fieldname: "action",
			files: this.files,
			task: this.task
		})

		this.dialog.$wrapper.find('.modal-dialog')
			.removeClass("modal-lg")
			.addClass("modal-xl")
			.removeClass("modal-dialog-scrollable");
		this.dialog.show()
	}
}

class TaskDetails {
	constructor(opts) {
		Object.assign(this, opts)
		this.field = this.dialog.get_field(this.fieldname)
		this.customer = frappe.boot.user_data.user_type == "Customer"

		this.sending = false;
		this.primary_file = []
		this.secondary_file = []

		this.make()
	}

	make() {
		this.field.$wrapper.html(`<div class="task-details"></div>`)
		let index_start = 1

		this.add_field_group()
		this.add_instructions()
		this.set_primary_action()

		if (this.task.download_step_label) {
			index_start = 0
			this.add_original_files()
		}

		if (this.task.primary_action_label) {
			this.show_primary_action_section()
			this.field.$wrapper.addClass("actions-timeline")
			this.field.$wrapper.find(".form-section").each(function(index) {
				index>index_start&&index<4&&$( this ).prepend(`<div class='section-number'>${index - index_start}</div>`)
				index==4&&$( this ).prepend(`<div class='section-number'>1</div>`)
			})
		} else {
			this.check_primary_action()
			this.enable_primary_action()
		}

		if (this.task.secondary_action) {
			this.show_secondary_action_section()
		}

		if (this.task.cancellation_action_label) {
			this.secondary_action = this.cancellation_action
			this.set_cancellation_action(this.task.cancellation_action_label)
		}
	}

	add_field_group() {
		this.form = new frappe.ui.FieldGroup({
			fields: this.get_fields(),
			body: this.field.$wrapper.find(".task-details"),
			no_submit_on_enter: true
		});
		this.form.make();
	}

	get_fields() {
		return [
			{
				fieldname: "instructions_section",
				fieldtype: "Section Break"
			},
			{
				fieldname: "original_instructions",
				fieldtype: "HTML"
			},
			{
				label: this.task.download_step_label || "",
				fieldname: "download_section",
				fieldtype: "Section Break",
				hidden: 1
			},
			{
				fieldname: "original_files",
				fieldtype: "HTML"
			},
			{
				label: this.task.primary_action_label || "",
				fieldname: "primary_action_section",
				fieldtype: "Section Break",
				hidden: 1
			},
			{
				fieldname: "primary_action",
				fieldtype: "HTML"
			},
			{
				label: this.task.secondary_action_label || "",
				fieldname: "secondary_action_section",
				fieldtype: "Section Break",
				hidden: 1
			},
			{
				fieldname: "secondary_action",
				fieldtype: "HTML"
			},
			{
				label: this.task.cancellation_action_instructions || "",
				fieldname: "cancellation_action_section",
				fieldtype: "Section Break",
				hidden: 1
			},
			{
				label: __("Rejection reason"),
				fieldname: "rejection_reasons",
				fieldtype: "Link",
				options: "Supplier Rejection Reason",
				onchange: () => {
					if (this.instructions_field) {
						this.instructions_field.df.placeholder = frappe.boot.rejection_reasons[this.form.get_value("rejection_reasons")]
						this.instructions_field.set_input_attributes()
					}
				},
				get_query: () => {
					return {
						'query': 'popcorn.utils.queries.get_rejection_reasons'
					};
				}
			},
			{
				fieldname: "cancellation_action",
				fieldtype: "HTML"
			}
		]
	}

	add_instructions() {
		this.form.get_field("original_instructions").$wrapper.append(`
			<div class="text-center head-title">
				${this.task.instructions || ""}
			</div>`)
	}

	add_original_files() {
		this.form.get_field("original_files").$wrapper.append(`
			<div class="original-files"></div>
		`)

		this.files.map(f => {
			const original_preview = $(`
				<div class="file-preview-area">
					<div class="file-preview-container">
						<div class="file-preview">
							<div class="file-icon">
								<div class="fallback">${frappe.utils.icon('file', 'md')}</div>
							</div>
							<div>
								<div>
									<span class="flex">
									<span class="file-name">${f.file_name}</span>
									</span>
								</div>
							</div>
							<div class="file-actions" style="cursor: pointer;">
								<div class="icon-md">
									<i class="fa fa-download fa-lg" aria-hidden="true"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
			`)

			original_preview.find(".file-actions").on("click", () => {
				this.download_file(f.name)
			})

			this.form.get_field("original_files").$wrapper.find(".original-files").append(original_preview)
		})

		this.show_section("download_section")
	}

	download_file(filename) {
		var w = window.open(
			frappe.urllib.get_full_url(
				"/api/method/popcorn.utils.file.download_file?"
				+"filename="+encodeURIComponent(filename)));
		if(!w) {
			frappe.msgprint(__("Please enable pop-ups")); return;
		}
	}

	show_primary_action_section() {
		this.show_section("primary_action_section")
	}

	show_secondary_action_section() {
		this.show_section("secondary_action_section")
	}

	upload_new_file(file_status, field) {
		const me = this;

		return new popcorn.ui.FileUploader({
			wrapper: this.form.get_field(field).$wrapper,
			doctype: "Task",
			docname: this.task.name,
			status: file_status,
			folder: `Home/${this.task.project}`,
			allow_multiple: false,
			restrictions: {
				allowed_file_types: ['application/pdf']
			},
			disable_file_browser: 1,
			show_upload_button: false,
			files: field == "primary_action" ? this.primary_file : this.secondary_file,
			on_success(file_doc) {
				//console.log("File uploaded", file_doc)
			},
			on_add(files) {
				if (field == "primary_action") {
					me.primary_file = files
				} else {
					me.secondary_file = files;
				}
				me.check_primary_action()
			}
		});
	}

	check_primary_action() {
		if (((this.task.secondary_action && this.secondary_file.length) || !this.task.secondary_action) && this.primary_file.length) {
			this.enable_primary_action()
		} else {
			this.disable_primary_action()
		}
	}

	cancellation_action() {
		//
	}

	new_task_action(action_name, customer_or_supplier) {
		if (!this.sending) {
			this.sending = true;
			frappe.xcall("popcorn.popcorn.doctype.task_action.task_action.create_new_task_action", {
				task: this.task.name,
				action_name: action_name,
				customer_or_supplier: customer_or_supplier,
				instructions: this.instructions_field&&this.instructions_field.get_value(),
				rejection_reasons: this.form.get_value("rejection_reasons")
			}).then(() => {
				popcorn.task_updates.trigger("refresh_tasks")
				this.sending = false;
				this.dialog.hide()
			})
		}
	}

	remove_primary_file() {
		popcorn.utils.delete_task_file(this.primary_file.docname).then(() => {
			popcorn.utils.delete_file(this.primary_file.document)
		})
		this.primary_file = []
		this.add_primary_upload_btn()
		this.action_btn.show()
		this.hide_primary_action()
	}

	set_primary_action() {
		this.dialog.set_primary_action(__("Submit"), () => {
			if (this.dialog.get_primary_btn().hasClass('disabled')) {
				return
			}
			this.submit_action()
		})
		this.disable_primary_action()
	}

	set_cancellation_action(label) {
		this.dialog.get_secondary_btn().unbind('click');
		this.dialog.set_secondary_action(() => {
			this.secondary_action()
		})
		this.dialog.set_secondary_action_label(label)
	}

	show_section(section, hide_other_sections) {
		const actions_sections = ["instructions_section", "download_section", "primary_action_section"]
		const secondary_section = ["secondary_action_section"]
		const cancellation_sections = ["cancellation_action_section"]

		const sections = this.task.secondary_action ? actions_sections.concat(cancellation_sections).concat(secondary_section) : actions_sections.concat(cancellation_sections)

		if (hide_other_sections) {
			sections.forEach(value => {
				this.form.set_df_property(value, "hidden", value !== section)
			})
		} else {
			this.form.set_df_property(section, "hidden", 0)
		}

		this.form.refresh_fields(hide_other_sections ? sections : [section]);
	}

	enable_primary_action() {
		this.dialog.enable_primary_action()
	}

	disable_primary_action() {
		this.dialog.disable_primary_action()
	}

	show_primary_action() {
		this.dialog.footer.removeClass('hide');
		this.dialog.get_primary_btn().removeClass("hide")
	}

	hide_primary_action() {
		this.dialog.footer.addClass('hide');
		this.dialog.get_primary_btn().addClass("hide")
	}
}

class CustomerTaskDetails extends TaskDetails {
	constructor(opts) {
		super(opts)
		Object.assign(this, opts)
	}

	show_primary_action_section() {
		super.show_primary_action_section()
		this.primary_uploader = this.upload_new_file("Optional", "primary_action")
	}

	check_primary_action() {
		this.submit_action = ["Verify", "Fix"].includes(this.task.action_name) ? this.unsuspend_task : this.archive_project
		super.check_primary_action()
	}

	archive_project() {
		frappe.xcall("popcorn.popcorn.doctype.project.project.archive_project", {project: this.task.project})
		.then(() => {
			popcorn.task_updates.trigger("refresh_tasks")
			this.dialog.hide()
		})
	}

	unsuspend_task() {
		const primary_upload = this.primary_uploader ? this.primary_uploader.upload_files() : null
		Promise.all([primary_upload]).then(() => {
			this.new_task_action(frappe.boot.tasks_map[this.task.step], "Supplier")
		})
	}

	cancellation_action() {
		console.log("Customer cancellation action")
	}
}

class SupplierTaskDetails extends TaskDetails{
	constructor(opts) {
		super(opts)
		Object.assign(this, opts)
		this.make_instructions_field()
	}

	make_instructions_field() {
		this.instructions_field = frappe.ui.form.make_control({
			parent: this.form.get_field("cancellation_action").$wrapper,
			df: {
				fieldname: "instructions",
				fieldtype: "Small Text",
				onchange: () => {
					const value = this.instructions_field.get_value()
					value ? this.enable_primary_action() : this.disable_primary_action()
				}
			},
			render_input: true
		});
	}

	show_primary_action_section() {
		super.show_primary_action_section()
		this.primary_uploader = this.upload_new_file("Primary", "primary_action")
	}

	show_secondary_action_section() {
		super.show_secondary_action_section()
		this.secondary_uploader = this.upload_new_file("Secondary", "secondary_action")
	}

	cancellation_action() {
		this.instructions_field.toggle(true)

		// Primary action
		this.submit_action = this.suspend_action
		this.set_primary_action()
		this.show_primary_action()

		// Secondary action
		this.secondary_action = this.back_action
		this.set_cancellation_action(__("Back"))

		this.show_section("cancellation_action_section", true)
	}

	back_action() {
		this.secondary_action = this.cancellation_action
		this.set_cancellation_action(this.task.cancellation_action_label)

		this.show_section("instructions_section", true)
		this.show_section("download_section")
		this.show_section("primary_action_section")
		this.secondary_action_label&&this.show_section("secondary_action_section")
	}

	check_primary_action() {
		this.submit_action = this.submit_task
		super.check_primary_action()
	}

	submit_task() {
		const primary_upload = this.primary_uploader ? this.primary_uploader.upload_files() : null
		const secondary_upload = this.secondary_uploader ? this.secondary_uploader.upload_files() : null

		Promise.all([primary_upload, secondary_upload]).then(() => {
			return frappe.xcall("popcorn.popcorn.doctype.task.task.open_next_task", {
				task: this.task.name
			})
		}).then(() => {
			popcorn.task_updates.trigger("refresh_tasks")
			this.dialog.hide()
		})
	}

	suspend_action() {
		this.new_task_action("Fix", "Customer")
	}
}