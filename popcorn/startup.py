import frappe
from frappe import _
from frappe.utils import markdown
from popcorn.popcorn.page.popcorn_profile.popcorn_profile import get_user_data

DAVRON_ROLES = ["Davron Digital Customer", "Davron Digital Supplier", "Guest", "All"]

TASK_SUPPLIER_STATUS_MAP = {
	"Notarization": "Notarize",
	"Apostille": "Apostille",
	"Translation": "Translate",
	"Legalization": "Legalize",
	"Franco Arabic Chamber of Commerce": "Certify",
	"Consulate": "Certify"
}

def boot_session(bootinfo):
	bootinfo.user_data = get_user_data()
	bootinfo.tasks_map = TASK_SUPPLIER_STATUS_MAP

	frappe.cache().hset("user_type", frappe.session.user, bootinfo.user_data["user_type"])
	frappe.cache().hset("suppliers", frappe.session.user, bootinfo.user_data["suppliers"])
	frappe.cache().hset("customer", frappe.session.user, bootinfo.user_data["customer"])

	bootinfo.type_of_documents = get_types_of_documents()
	bootinfo.type_of_documents_examples = get_types_of_documents_examples()

	bootinfo.rejection_reasons = get_rejection_reasons()

	bootinfo.tezos_transaction_url = frappe.conf.tezos_transaction_url or "https://tezblock.io/transaction/"

	check_davron_roles()
	disable_all_roles()

def check_davron_roles():
	for role in DAVRON_ROLES:
		role_doc = {
			"doctype": "Role",
			"role_name": role,
			"home_page": "/app/project",
			"desk_access": 1,
			"search_bar": 0,
			"notifications": 0,
			"chat": 0,
			"list_sidebar": 0,
			"bulk_actions": 0,
			"view_switcher": 0,
			"form_sidebar": 1,
			"timeline": 0,
			"dashboard": 0
		}
		if not frappe.db.exists("Role", role):
			frappe.get_doc(role_doc).insert(ignore_permissions=True)
		else:
			existing_role = frappe.get_doc("Role", role)
			for key in role_doc:
				if existing_role.get(key) != role_doc.get(key):
					frappe.db.set_value("Role", role, key, role_doc.get(key))

def disable_all_roles():
	active_roles = ["Davron Digital Customer", "Davron Digital Supplier", "System Manager", "Guest"]
	for role in frappe.get_all("Role",filters={"name": ("not in", active_roles), "disabled": 0}, pluck="name"):
		frappe.db.set_value("Role", role, "disabled", 1)

def get_types_of_documents():
	field = "description_fr" if frappe.local.lang == "fr" else "description_en"
	doclist = {x.type_of_document: markdown(x.get(field)) for x in frappe.get_all("Type of document", fields=["type_of_document", field])}
	return doclist

def get_types_of_documents_examples():
	doclist = {x.example: x.type_of_document for x in frappe.get_all("Type of document example", fields=["example", "type_of_document"])}
	return doclist

def get_rejection_reasons():
	doclist = {x.rejection_reason: _(x.placeholder) for x in frappe.get_all("Supplier Rejection Reason", fields=["rejection_reason", "placeholder"])}
	return doclist

def on_login():
	frappe.db.set_default('desktop:home_page', 'homepage')