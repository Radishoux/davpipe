frappe.provide("popcorn.setup");
frappe.provide("frappe.ui");
frappe.provide("popcorn.task_updates")
frappe.utils.make_event_emitter(popcorn.task_updates);

frappe.listview_settings['Project'] = {
	hide_name_column: true,
	has_indicator_for_draft: true,
	get_indicator: function(doc) {
		let color = "light-gray"
		if (doc.status == "Cancelled") {
			color = "red";
		} else if (doc.status == "Archived") {
			color = "gray";
		} else if (doc.status == "Active") {
			color = "blue";
		} else if (doc.status == "Suspended") {
			color = "yellow";
		} else if (doc.status == "Late") {
			color = "orange";
		} else if (doc.status == "Completed") {
			color = "green";
		}

		return [__(doc.status), color];
	},
	formatters: {
		type_of_document(value) {
			return __(value);
		}
	},
	button: {
		show: function(doc) {
			return doc.current_task&&doc.current_task.action_name;
		},
		get_label: function(doc) {
			return __(doc.current_task.action_name);
		},
		get_description: function(doc) {
			// return __(doc.current_task.instructions);
		},
		action: function(doc) {
			new popcorn.task_action({task: doc.current_task})
		}
	},
	onload: (list_view) => {
		$(document.body).toggleClass("no-list-sidebar", true)
		$(".sidebar-toggle-btn").hide();
		list_view.page.menu_btn_group.hide();

		list_view.page.page_form.find('[data-fieldname="name"]').hide()
		list_view.page.page_form.find('.filter-selector').hide()
		list_view.method = "popcorn.popcorn.doctype.project.project.get_list"

		$(list_view.page.page_actions).find(`[data-label="Add ${__("Project")}"]`).text(__("Create a project"))
		list_view.page.set_primary_action = () => {
			$(list_view.page.page_actions).find(`[data-label="Add ${__("Project")}"]`).text(__("Create a project"))
		}

		popcorn.task_updates.on("refresh_tasks", () => {
			list_view.refresh();
		})

		if (frappe.boot.user_data.customer && frappe.boot.user_data.customer.name) {
			frappe.db.get_list("Project", {filters: {customer: frappe.boot.user_data.customer.name}, limit: 1, order_by: "modified desc", fields: ["project_name", "modified"]}).then(
				r => {
					if (r.length) {
						list_view.page.main.before(`
							<div class="mb-2 d-flex flex-row">
								<div class="frappe-card d-flex">
									<div class="px-2" style="border-right: 1px solid #8D99A6">
										<div class="text-muted">${__("Last project updated")}</div>
										<div class="control-label">${r[0].project_name}</div>
									</div>
									<div class="px-2">
										<div class="text-muted">${__("Last update")}</div>
										<div class="control-label">${frappe.datetime.obj_to_user(r[0].modified)}</div>
									</div>
								</div>
							</div>
						`)
					}
				}
			)
		}
	},
	primary_action: () => {
		frappe.set_route("new-project")
	},
	get_form_link(doc) {
		const docname = doc.name.match(/[%'"\s]/)
			? encodeURIComponent(doc.name)
			: doc.name;

		if (doc.docstatus == 0) {
			return `/app/new-project/${docname}`
		} else {
			return `/app/form/project/${docname}`;
		}
	}
}
