# -*- coding: utf-8 -*-
# Copyright (c) 2020, Davron Digital and contributors
# For license information, please see license.txt

import unicodedata
from datetime import datetime

import numpy as np
import pandas as pd

import frappe
from frappe import _
from frappe.model.document import Document
from frappe.utils import nowdate, add_days, getdate
from frappe.desk.reportview import get
from popcorn.popcorn.doctype.task_action.data import TASK_ACTION_MAP
from popcorn.install.countries import ARAB_LEAGUE
from popcorn.utils.audit import create_audit_log, set_project_status, set_task_status, set_task_action_status
from popcorn.utils.notifications import PopcornSupplierNotifications, PopcornCustomerNotifications

STATUS_MAP = {
	0: "Draft",
	2: "Cancelled"
}

from pandas.tseries.holiday import AbstractHolidayCalendar, Holiday, EasterMonday, Easter
from pandas.tseries.offsets import Day, CustomBusinessDay

class FrBusinessCalendar(AbstractHolidayCalendar):
	""" Custom Holiday calendar for France based on
		https://en.wikipedia.org/wiki/Public_holidays_in_France
	  - 1 January: New Year's Day
	  - Moveable: Easter Monday (Monday after Easter Sunday)
	  - 1 May: Labour Day
	  - 8 May: Victory in Europe Day
	  - Moveable Ascension Day (Thursday, 39 days after Easter Sunday)
	  - 14 July: Bastille Day
	  - 15 August: Assumption of Mary to Heaven
	  - 1 November: All Saints' Day
	  - 11 November: Armistice Day
	  - 25 December: Christmas Day
	"""
	rules = [
		Holiday('New Years Day', month=1, day=1),
		EasterMonday,
		Holiday('Labour Day', month=5, day=1),
		Holiday('Victory in Europe Day', month=5, day=8),
		Holiday('Ascension Day', month=1, day=1, offset=[Easter(), Day(39)]),
		Holiday('Bastille Day', month=7, day=14),
		Holiday('Assumption of Mary to Heaven', month=8, day=15),
		Holiday('All Saints Day', month=11, day=1),
		Holiday('Armistice Day', month=11, day=11),
		Holiday('Christmas Day', month=12, day=25)
	]

class Project(Document):
	def before_insert(self):
		normalized_name = unicodedata.normalize('NFD', self.customer).encode('ascii', 'ignore').decode('utf-8')
		self.project_prefix = frappe.scrub(normalized_name).replace("'", "_")

	def after_insert(self):
		self.create_file_folder()
		create_audit_log("Project Creation", self.as_dict(), self.name, frappe.session.user)

	def before_save(self):
		if self.type_of_legalization:
			self.set_dates()
			self.add_task_and_dates(simulate=True)

	def on_update_after_submit(self):
		create_audit_log("Project Modification", self.as_dict(), self.name, frappe.session.user)

	def on_submit(self):
		self.set_status()
		self.add_task_and_dates(simulate=False)
		create_audit_log("Project Submission", self.as_dict(), self.name, frappe.session.user)
		PopcornCustomerNotifications("project_created", self.name)

	def add_task_and_dates(self, simulate=False):
		self.tasks = []
		self.create_tasks(simulate=simulate)
		self.set_expected_task_dates(simulate=simulate)

	def on_cancel(self):
		for task in frappe.get_all("Task", filters={"project": self.name, "status": ("not in", ("Completed", "Cancelled"))}):
			set_task_status(task.name, "Cancelled")

			for task_action in frappe.get_all("Task Action", filters={"task": task.name, "status": "Started"}):
				set_task_action_status(task_action.name, "Cancelled")

	def on_trash(self):
		if self.docstatus == 0 or frappe.session.user == "Administrator" or "System Manager" in frappe.get_roles():
			for task in frappe.get_all("Task", filters={"project": self.name}):
				for task_action in frappe.get_all("Task Action", filters={"task": task.name}):
					frappe.get_doc("Task Action", task_action.name).delete()

				frappe.get_doc("Task", task.name).delete()

	def validate(self):
		if self.has_value_changed("country_of_destination"):
			self.type_of_legalization = None

		if self.country_of_destination in ARAB_LEAGUE and self.type_of_document == "Private deed (only with certification of signature)":
			self.certification_by_ccfa = 1
		else:
			self.certification_by_ccfa = 0

		if self.country_of_destination and self.country_of_destination != "France" and self.type_of_document:
			self.get_type_of_legalization(self.country_of_destination)

		elif self.country_of_destination and self.country_of_destination == "France" and self.issuing_country and self.type_of_document:
			self.get_type_of_legalization(self.issuing_country)

	def get_type_of_legalization(self, country):
		rules = get_legalization_rules(country, self.type_of_document)
		if rules:
			rule = rules[0]

			if rule.type_of_legalization in ("Apostille", "Legalization", "Exemption"):
				self.type_of_legalization = rule.type_of_legalization

			elif rule.case_values and self.legalization_question_1:
				if rule.case_values.get(f"{self.legalization_question_1.lower()}_field") in ("Apostille", "Legalization", "Exemption"):
					self.type_of_legalization = rule.case_values.get(f"{self.legalization_question_1.lower()}_field")

				elif rule.secondary_case_values and self.legalization_question_2:
					if rule.secondary_case_values.get(f"{self.legalization_question_2.lower()}_field") in ("Apostille", "Legalization", "Exemption"):
						self.type_of_legalization = rule.secondary_case_values.get(f"{self.legalization_question_2.lower()}_field")

			if not self.type_of_legalization:
				frappe.throw(_("The type of legalization could not be determined for your document. Please contact the support."))

	def set_status(self):
		if self.docstatus != 1:
			status = STATUS_MAP.get(self.docstatus)
		else:
			status = "Active"

		self.db_set("status", status)

	def set_dates(self):
		self.start_date = nowdate()

	def set_expected_task_dates(self, simulate=False):
		previous_date = self.start_date
		for index, task in enumerate(self.tasks):
			if simulate:
				task.assign_supplier()

			expected_start_date = expected_end_date = None
			# if task.step_number == 0:
			# 	start_date = previous_date

			expected_start_date = np.busday_offset(previous_date, 0, roll='forward')
			offset = frappe.db.get_value("Supplier", task.supplier, "delivery_timeframe") or 0
			res = (pd.offsets.CustomBusinessDay(calendar=FrBusinessCalendar()) * offset).apply(expected_start_date)
			expected_end_date = res.to_pydatetime()
			previous_date = getdate(expected_end_date)

			if not simulate:
				frappe.db.set_value("Task", task.name, "expected_start_date", getdate(expected_start_date.astype(datetime)))
				frappe.db.set_value("Task", task.name, "expected_end_date", getdate(expected_end_date))

				if task.step_number == 0:
					PopcornSupplierNotifications("task_is_available", self.name, task.name)

		if index == len(self.tasks) - 1:
			if not simulate:
				self.db_set("expected_end_date", previous_date)
			else:
				self.expected_end_date = previous_date

	def create_tasks(self, simulate=False):
		flow = 1
		if self.type_of_legalization == "Legalization":
			flow = 2
			if self.translate_doc and self.certification_by_ccfa:
				flow = 6
			elif self.translate_doc:
				flow = 4
			elif self.certification_by_ccfa:
				flow = 5

		elif self.type_of_legalization == "Apostille":
			if self.translate_doc:
				flow = 3

		elif self.type_of_legalization == "Exemption":
			if self.type_of_notarization == "None" and self.translate_doc:
				flow = 8
			else:
				flow = 7

		self.add_tasks(flow=flow, simulate=simulate)

	def add_tasks(self, flow, simulate=False):
		tasks_map = {
			1: ["Notarization", "Apostille"],
			2: ["Notarization", "Legalization", "Consulate"],
			3: ["Notarization", "Translation", "Notarization", "Apostille"],
			4: ["Notarization", "Translation", "Notarization", "Legalization", "Consulate"],
			5: ["Notarization", "Legalization", "Franco Arabic Chamber of Commerce", "Consulate"],
			6: ["Notarization", "Translation", "Notarization", "Legalization", "Franco Arabic Chamber of Commerce", "Consulate"],
			7: ["Notarization", "Translation"],
			8: ["Translation"]
		}

		for index, step in enumerate(tasks_map.get(flow)):
			task = frappe.get_doc({
				"doctype": "Task",
				"project": self.name,
				"step": step,
				"step_number": index,
				"status": "Active" if index == 0 else "Not Started"
			})
			
			if not simulate:
				task.insert(ignore_permissions=True)

			self.tasks.append(task)

	def create_file_folder(self):
		frappe.get_doc({
			"doctype": "File",
			"file_name": self.name,
			"is_folder": 1,
			"folder": "Home"
		}).insert(ignore_permissions=True)


@frappe.whitelist()
def get_list():
	user_type = frappe.cache().hget('user_type', frappe.session.user)
	suppliers = []
	customer = None

	frappe.local.form_dict.filters = frappe.parse_json(frappe.local.form_dict.filters) if frappe.local.form_dict.filters else []

	if user_type == "Admin":
		suppliers = frappe.get_all("Supplier", pluck="name")

	elif user_type == "Supplier":
		suppliers = frappe.cache().hget('suppliers', frappe.session.user)
		frappe.local.form_dict.filters.append(["Project","docstatus","=","1"])
	else:
		customer = frappe.cache().hget('customer', frappe.session.user)
		if customer:
			frappe.local.form_dict.filters.append(["Project","customer","=",customer.name])
			frappe.local.form_dict.filters.append(["Project","docstatus","<","2"])

	if not customer and not suppliers:
		return []

	project_list = get()

	if project_list:
		conditions = "ta.customer_action = 1" if user_type == "Customer" else f'ta.supplier_action = 1 and t.supplier in ({",".join("{0}".format(frappe.db.escape(x)) for x in suppliers)})'

		task_list = frappe.db.sql(f"""
			SELECT
				ta.project,
				ta.action_name,
				ta.customer_action,
				ta.supplier_action,
				ta.instructions,
				t.*
			FROM `tabTask Action` as ta
			JOIN `tabTask` as t ON t.name = ta.task
			WHERE ta.status = 'Started'
			AND {conditions}
		""", as_dict=True)

		project_list["keys"].append("current_task")

		for value in project_list.get("values", []):
			tasks = [x for x in task_list if x.project == value[0]]
			if tasks:
				value.append(tasks[0].update(TASK_ACTION_MAP.get(tasks[0].action_name)))

	return project_list

@frappe.whitelist()
def get_project_files(project):
	if not project:
		return []

	files = frappe.get_all("File", filters={
		"attached_to_doctype": "Project",
		"attached_to_name": project
	}, fields=["name", "file_name", "file_url", "creation"])

	return files

@frappe.whitelist()
def archive_project(project):
	set_project_status(project, "Archived")

	for task in frappe.get_all("Task", filters=dict(project=project, status=("in", ("Not Started", "Active", "Late", "Suspended")))):
		set_task_status(task.name, "Completed")

	for task_action in frappe.get_all("Task Action", filters=dict(project=project)):
		set_task_action_status(task_action.name, "Completed")

@frappe.whitelist()
def get_legalization_rules(country, type_of_document=None):
	filters = {"country": country}

	if type_of_document:
		filters.update({"type_of_document": type_of_document})

	country_rules = frappe.get_all("Legalization Rule",
		filters,
		["type_of_document", "type_of_legalization", "case_name"])

	def get_case_values(case):
		return frappe.db.get_value("Legalization Exception", case, ["legalization_question_text", "yes_field", "no_field", "secondary_case"], as_dict=True)

	for rule in country_rules:
		if rule.case_name:
			rule["case_values"] = get_case_values(rule.case_name) or {}
			if rule["case_values"].get("secondary_case"):
				rule["secondary_case_values"] = get_case_values(rule["case_values"].get("secondary_case"))

	return country_rules

def delete_old_drafts():
	projects = frappe.get_all("Project", filters={"modified": (">", add_days(nowdate(), 30)), "docstatus": 0})

	for project in projects:
		frappe.delete_doc("Project", project.name)