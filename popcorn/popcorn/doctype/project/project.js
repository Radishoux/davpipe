// Copyright (c) 2020, Davron Digital and contributors
// For license information, please see license.txt
frappe.provide("popcorn.ui")
frappe.provide("popcorn.task_updates")
frappe.utils.make_event_emitter(popcorn.task_updates);

frappe.ui.form.on('Project', {
	setup(frm) {
		popcorn.task_updates.on("refresh_tasks", () => {
			frm.reload_doc();
		})
	},
	refresh(frm) {
		frm.page.hide_menu()
		frm.page.standard_actions.find(".page-icon-group").hide()

		frm.sidebar.sidebar.empty();

		return frappe.xcall("popcorn.popcorn.doctype.task.task.get_tasks", {
			project: frm.docname
		}).then(r => {
			frm.sidebar = new popcorn.ui.FormSidebar({
				frm: frm,
				page: frm.page,
				tasks: r
			});
			frm.sidebar.make();

			frm.project_feed = new popcorn.ui.ProjectFeed({
				frm: frm,
				tasks: r,
				fields_dict: frm.fields_dict,
				parent: frm.page.main,
				doc: frm.doc
			})
		});
	}
});