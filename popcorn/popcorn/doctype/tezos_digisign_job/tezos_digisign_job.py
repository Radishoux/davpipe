# -*- coding: utf-8 -*-
# Copyright (c) 2021, Davron Digital and contributors
# For license information, please see license.txt

import frappe
from frappe.utils import get_datetime
from frappe.model.document import Document

from popcorn.utils.tezos import TezosJobs, TezosFile, send_to_tezos

class TezosDigiSignJob(Document):
	def validate(self):
		self.set_status(udpdate=False)

	def set_status(self, udpdate=True):
		if self.status == "Rejected":
			return

		status = "Inserted"
		if self.injection_datetime:
			status = "Injected"
		if self.validation_datetime:
			status = "Validated"

		if udpdate:
			self.db_set("status", status)
		else:
			self.status = status

def check_digisign_job_status():
	jobs = frappe.get_all("Tezos DigiSign Job", filters={"block_hash": ("is", ("not set")), "status": ("!=", "Rejected")}, pluck="name")

	tezos_api = TezosJobs()
	for job in jobs:
		tezos_job = tezos_api.get(job)

		if tezos_job.status_code == 200:
			digisign_job = tezos_job.json()

			if digisign_job.get("state") == "REJECTED":
				frappe.db.set_value("Tezos DigiSign Job", job, "status", "Rejected")

			popcorn_job = frappe.get_doc("Tezos DigiSign Job", job)
			popcorn_job.insertion_datetime = get_datetime(digisign_job.get("createdDate")).replace(tzinfo=None)
			popcorn_job.injection_datetime = get_datetime(digisign_job.get("injectedDate")).replace(tzinfo=None) if digisign_job.get("injectedDate") else None
			popcorn_job.validation_datetime = get_datetime(digisign_job.get("validatedDate")).replace(tzinfo=None) if digisign_job.get("validatedDate") else None
			popcorn_job.block_hash = digisign_job.get("blockHash")
			popcorn_job.transaction_hash = digisign_job.get("transactionHash")
			popcorn_job.save()

	frappe.publish_realtime("refresh_project")


def get_digisign_proof():
	jobs = frappe.get_all("Tezos DigiSign Job",
		filters={"digisign_proof": ("is", ("not set")), "status": ("not in", ("Rejected", "Retried"))},
		fields=["name", "merkeltree"]
	)

	tezos_api = TezosJobs()
	for job in jobs:
		files = tezos_api.get_files(job.name)
		proofs = []

		if files.status_code == 200:
			for file in files.json():
				f = frappe._dict(file)
				res = TezosFile().get_proof(f.id)
				if res.status_code == 200:
					proofs.append(res.json())

			if proofs:
				frappe.db.set_value("Tezos DigiSign Job", job.name, "digisign_proof", frappe.as_json(proofs, indent=4))

		if not job.merkeltree:
			merkel_tree = tezos_api.get_merkelTree(job.name)
			if merkel_tree.status_code == 200:
				frappe.db.set_value("Tezos DigiSign Job", job.name, "merkeltree", frappe.as_json(merkel_tree.json(), indent=4))

@frappe.whitelist()
def retry(job):
	files = frappe.parse_json(frappe.db.get_value("Tezos DigiSign Job", job, "files"))

	frappe.db.set_value("Tezos DigiSign Job", job, "status", "Retried")

	return send_to_tezos(files=files)