// Copyright (c) 2021, Davron Digital and contributors
// For license information, please see license.txt

frappe.ui.form.on('Tezos DigiSign Job', {
	refresh: function(frm) {
		if (["Inserted"].includes(frm.doc.status)) {
			frm.add_custom_button(__("Re-send to Tezos"), function() {
				return frappe.call({
					method: "popcorn.popcorn.doctype.tezos_digisign_job.tezos_digisign_job.retry",
					args: {
						job: frm.doc.name
					},
					callback: function(r) {
						frappe.show_alert({
							message: __('Job Retried'),
							indicator: 'info'
						});
						frm.reload_doc()
					}
				});
			});
		}
	}
});
