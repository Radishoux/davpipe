// Copyright (c) 2020, Davron Digital and contributors
// For license information, please see license.txt

// TODO: convert to working days
const delivery_timeframe_map = {
	"Ministry of Foreign Affairs": 2,
	"Consulate": 15,
	"Translator": 5,
	"Court of Appeals": 2,
	"Notary": 1,
	"Chamber of Commerce": 1,
	"Franco Arabic Chamber of Commerce": 2
}

frappe.ui.form.on('Supplier', {
	setup: function(frm) {
		frm.set_query("user", "supplier_users", function() {
			return {
				query: "frappe.core.doctype.user.user.user_query",
				filters: {
					ignore_user_type: 1
				}
			}
		});
	},
	supplier_type: function(frm) {
		if (frm.doc.supplier_type) {
			frm.set_value("delivery_timeframe", delivery_timeframe_map[frm.doc.supplier_type]);
		}
	}
});
