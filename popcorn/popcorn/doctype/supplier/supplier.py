# -*- coding: utf-8 -*-
# Copyright (c) 2020, Davron Digital and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class Supplier(Document):
	def validate(self):
		for supplier in self.supplier_users:
			user = frappe.get_doc("User", supplier.user)
			user.remove_disabled_roles()
			user.add_roles("Davron Digital Supplier")
			user.set_system_user()