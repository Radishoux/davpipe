from frappe import _

TASK_ACTION_MAP = {
	"Fix": {
		"dialog_title": "",
		"primary_action_label": _("Please add the requested file")
	},
	"Verify": {
		"dialog_title": "",
		"download_step_label": "",
		"primary_action_label": _("Verify"),
		"secondary_action": "",
		"secondary_action_label": "",
		"cancellation_action_label": _("Reject"),
		"cancellation_action_instructions": _("Reason for the rejection"),
		"instructions": _("An inconsistency has been detected between the source file and your signed file. Please correct your action")
	},
	"Archive": {
		"instructions": _("The project has been delivered. Please archive it.")
	},
	"Notify": {
		"dialog_title": "",
		"download_step_label": "",
		"primary_action_label": "",
		"secondary_action": "",
		"secondary_action_label": "",
		"cancellation_action_label": "",
		"instructions": ""
	},
	"Legalize": {
		"dialog_title": _("Legalization"),
		"download_step_label": _("Download the source file"),
		"primary_action_label": _("Upload the legalized file"),
		"cancellation_action_label": _("Suspend"),
		"cancellation_action_instructions": _("Reason for the suspension of your action"),
		"instructions": _("Please add your legalized and signed file")
	},
	"Translate": {
		"dialog_title": _("Translation"),
		"download_step_label": _("Download the source file"),
		"primary_action_label": _("Upload the signed source file"),
		"secondary_action": "Translation",
		"secondary_action_label": _("Upload the signed translation"),
		"cancellation_action_label": _("Suspend"),
		"cancellation_action_instructions": _("Reason for the suspension of your action"),
		"instructions": _("Please add your two distinct signed files: the source and the translation as an apendix & indicate in your translation the number of 'vu ne varietur' & the hash of the source document")
	},
	"Apostille": {
		"dialog_title": _("Apostille"),
		"download_step_label": _("Download the source file"),
		"primary_action_label": _("Upload the signed source file"),
		"secondary_action": "Apostille",
		"secondary_action_label": _("Upload the signed apostille"),
		"cancellation_action_label": _("Suspend"),
		"cancellation_action_instructions": _("Reason for the suspension of your action"),
		"instructions": _("Please add your two distinct signed files: the source and the apostille mentionning the hash of the source document")
	},
	"Certify": {
		"dialog_title": _("Certification"),
		"download_step_label": _("Download the source file"),
		"primary_action_label": _("your certified and signed file"),
		"cancellation_action_label": _("Suspend"),
		"cancellation_action_instructions": _("Reason for the suspension of your action"),
		"instructions": _("Please add your certified and signed file")
	},
	"Notarize": {
		"dialog_title": _("Notarization"),
		"download_step_label": _("Download the source file"),
		"primary_action_label": _("Upload the notarized file"),
		"cancellation_action_label": _("Suspend"),
		"cancellation_action_instructions": _("Reason for the suspension of your action"),
		"instructions": _("Please add your notarized and signed file")
	},
}