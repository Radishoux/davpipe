frappe.listview_settings['Task Action'] = {
	hide_name_column: true,
	get_indicator: function(doc) {
		let color = "darkgrey";
		if (doc.status == "Started") {
			color = "orange";
		} else if (doc.status == "Completed") {
			color = "green";
		}

		return [__(doc.status), color];
	}
}