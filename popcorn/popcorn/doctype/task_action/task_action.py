# -*- coding: utf-8 -*-
# Copyright (c) 2020, Davron Digital and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from frappe.model.document import Document
from popcorn.popcorn.doctype.task_action.data import TASK_ACTION_MAP
from popcorn.utils.audit import create_audit_log, set_project_status, set_task_status, set_task_action_status
from popcorn.utils.notifications import PopcornSupplierNotifications, PopcornCustomerNotifications

STATUS_MAP = {
	"Fix": "Suspended",
	"Archive": "Completed"
}

class TaskAction(Document):
	def before_insert(self):
		self.close_previous_actions()
		self.format_instructions()

	def after_insert(self):
		create_audit_log("Task Action Creation", self.as_dict(), self.project, frappe.session.user)

		if self.action_name == "Fix":
			PopcornCustomerNotifications("project_suspended", self.project, self.task, self.rejection_reasons)

	def on_update(self):
		create_audit_log("Task Action Modification", self.as_dict(), self.project, frappe.session.user)

	def close_previous_actions(self):
		for action in frappe.get_all("Task Action",
			filters={"task": self.task, "name": ("!=", self.name), "status": "Started"},
			fields=["name", "action_name", "status", "project", "task", "rejection_reasons", "creation"]):

			if action.action_name == "Fix":
				PopcornSupplierNotifications("task_unsuspended", action.project, action.task, action.rejection_reasons)
				PopcornCustomerNotifications("project_unsuspended", action.project, action.task, suspension_date=action.creation)
			set_task_action_status(action.name, "Completed")

	def format_instructions(self):
		if self.action_name == "Fix":
			self.instructions = (
				f'<div class="text-center"><h3>{self.rejection_reasons or ""}</h3>\n'
				f'<p>{_("The task could not be fulfilled:")} {self.instructions}</p></div>'
			)


@frappe.whitelist()
def create_new_task_action(task, action_name, customer_or_supplier, instructions=None, rejection_reasons=None):
	status = STATUS_MAP.get(action_name, "Active")
	project = frappe.db.get_value("Task", task, "project")

	set_task_status(task, status)
	set_project_status(project, status)

	return frappe.get_doc({
		"doctype": "Task Action",
		"task": task,
		"action_name": action_name,
		"supplier_action": customer_or_supplier == "Supplier",
		"customer_action": customer_or_supplier == "Customer",
		"instructions": instructions or TASK_ACTION_MAP.get(action_name, {}).get("instructions") or "",
		"rejection_reasons": rejection_reasons
	}).insert(ignore_permissions=True)