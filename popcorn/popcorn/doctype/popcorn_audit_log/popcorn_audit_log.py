# -*- coding: utf-8 -*-
# Copyright (c) 2021, Davron Digital and contributors
# For license information, please see license.txt

import json

import frappe
from frappe.model.document import Document
from popcorn.utils.audit import get_content_hash

class PopcornAuditLog(Document):
	def before_insert(self):
		if not self.hash_key:
			self.hash_key = get_content_hash(frappe.as_json({
				"name": self.name,
				"log_type": self.log_type,
				"value": self.value,
				"project": self.project,
				"user": self.user,
				"creation": self.creation
			}))
