// Copyright (c) 2020, Davron Digital and contributors
// For license information, please see license.txt

frappe.ui.form.on('Customer', {
	setup: function(frm) {
		frm.set_query("user", "users", function() {
			return {
				query: "frappe.core.doctype.user.user.user_query",
				filters: {
					ignore_user_type: 1
				}
			}
		});

		frm.set_query("supplier", "suppliers", function(frm, cdt, cdn) {
			const row = locals[cdt][cdn]
			return {
				filters: {
					supplier_type: row.supplier_type
				}
			}
		});
	},
	refresh: function(frm) {
		if(!frm.doc.__islocal) {
			frappe.contacts.render_address_and_contact(frm);
		}
	}
});
