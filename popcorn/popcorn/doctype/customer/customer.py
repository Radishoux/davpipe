# -*- coding: utf-8 -*-
# Copyright (c) 2020, Davron Digital and contributors
# For license information, please see license.txt

import functools

import frappe
from frappe import _
from frappe.model.document import Document

class Customer(Document):
	def onload(self):
		load_address_and_contact(self)

	def validate(self):
		for customer in self.users:
			user = frappe.get_doc("User", customer.user)
			user.remove_disabled_roles()
			user.add_roles("Davron Digital Customer")
			user.set_system_user()


def load_address_and_contact(doc):
	from frappe.contacts.doctype.address.address import get_address_display, get_condensed_address

	filters = [
		["Dynamic Link", "link_doctype", "=", doc.doctype],
		["Dynamic Link", "link_name", "=", doc.name],
		["Dynamic Link", "parenttype", "=", "Address"],
		["Disabled", "=", False]
	]
	address_list = frappe.get_all("Address", filters=filters, fields=["*"])

	address_list = [a.update({"display": get_address_display(a)})
		for a in address_list]

	address_list = sorted(address_list,
		key = functools.cmp_to_key(lambda a, b:
			(int(a.is_primary_address - b.is_primary_address)) or
			(1 if a.modified - b.modified else 0)), reverse=True)

	doc.set_onload('addr_list', address_list)