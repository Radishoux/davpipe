# -*- coding: utf-8 -*-
# Copyright (c) 2020, Davron Digital and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from frappe.model.document import Document
from frappe.utils import nowdate, format_date, now_datetime, getdate, cint
from popcorn.startup import TASK_SUPPLIER_STATUS_MAP
from popcorn.popcorn.page.popcorn_profile.popcorn_profile import get_user_data
from popcorn.popcorn.doctype.task_action.data import TASK_ACTION_MAP
from popcorn.popcorn.doctype.task_action.task_action import create_new_task_action
from popcorn.utils.audit import create_audit_log, set_project_status, set_task_action_status
from popcorn.utils.notifications import PopcornSupplierNotifications, PopcornCustomerNotifications

PERMISSION_MAP = {
	"Ministry of Foreign Affairs": "Legalization",
	"Consulate": "Consulate",
	"Translator": "Translation",
	"Court of Appeals": "Apostille",
	"Notary": "Notarization",
	"Chamber of Commerce": "Notarization",
	"Franco Arabic Chamber of Commerce": "Franco Arabic Chamber of Commerce"
}

class Task(Document):
	def before_insert(self):
		self.assign_supplier()
		if self.step_number == 0:
			self.start_date = now_datetime()

	def after_insert(self):
		if self.step_number == 0:
			self.create_task_action()
			self.add_original_documents_to_task(self.name)

		create_audit_log("Task Creation", self.as_dict(), self.project, frappe.session.user)

	def on_update(self):
		create_audit_log("Task Modification", self.as_dict(), self.project, frappe.session.user)

	def create_task_action(self):
		frappe.get_doc({
			"doctype": "Task Action",
			"task": self.name,
			"action_name": TASK_SUPPLIER_STATUS_MAP.get(self.step),
			"supplier_action": 1,
			"instructions": TASK_ACTION_MAP.get(TASK_SUPPLIER_STATUS_MAP.get(self.step), {}).get("instructions") or ""
		}).insert(ignore_permissions=True)

	def assign_supplier(self):
		country = None
		if self.step == "Notarization":
			if frappe.db.get_value("Project", self.project, "type_of_document_example") == "Medical certificate":
				supplier_type = "French Medical Association"
			else:
				type_of_notarization = frappe.db.get_value("Project", self.project, "type_of_notarization")
				supplier_type = "Notary" if type_of_notarization == "Notary" else "Chamber of Commerce"

		elif self.step == "Consulate":
			country = frappe.db.get_value("Project", self.project, "country_of_destination")
			supplier_type = "Consulate"

		else:
			reversed_permission_map = {value : key for (key, value) in PERMISSION_MAP.items()}
			supplier_type = reversed_permission_map.get(self.step)

		if supplier_type:
			self.supplier = self.get_supplier(supplier_type, country)

	def get_supplier(self, supplier_type, country=None):
		supplier = None
		filters = {"supplier_type": supplier_type}
		if country:
			filters.update({"country": country})
		else:
			supplier = frappe.db.get_value("Customer Suppliers", dict(
				parent=frappe.db.get_value("Project", self.project, "customer"),
				supplier_type = supplier_type
			), "supplier")

		return supplier or frappe.db.get_value("Supplier", dict(filters, **{"default_supplier": 1})) or frappe.db.get_value("Supplier", filters)

	def set_status(self, status):
		if status == "Completed":
			self.db_set("end_date", now_datetime())
			self.db_set("completed_by", frappe.session.user)

		if status == "Active" and getdate(nowdate()) > getdate(self.expected_end_date):
			status = "Late"

		if status == "Late":
			set_project_status(self.project, "Late")

		self.db_set("status", status)

	def close_task(self, next_task=None):
		self.set_status("Completed")
		for action in frappe.get_all("Task Action", filters={"task": self.name, "status": "Started"}):
			set_task_action_status(action.name, "Completed")

		if next_task:
			self.add_original_documents_to_task(next_task.name)
		else:
			set_project_status(self.project, "Completed")
			create_new_task_action(self.name, "Archive", "Customer", _("The project has been delivered to {0} on {1}").format("customer@davrondigital.com", format_date(nowdate())))

			if cint(frappe.db.get_value("Project", self.project, "send_documents")):
				PopcornCustomerNotifications("project_completed_hard_copy", self.project, self.name)
			else:
				PopcornCustomerNotifications("project_completed_digital_only", self.project, self.name)


	def open_task(self):
		self.set_status("Active")
		self.db_set("start_date", now_datetime())
		self.create_task_action()
		PopcornSupplierNotifications("task_is_available", self.project, self.name)

	def add_original_documents_to_task(self, task_name):
		original_document = frappe.db.get_value("Project", self.project, "original_document")
		if original_document:
			project_original = frappe.get_doc("File", original_document)
			task_original = frappe.copy_doc(project_original, ignore_no_copy = False)
			task_original.attached_to_doctype = "Task"
			task_original.attached_to_name = task_name
			task_original.insert(ignore_permissions=True)

@frappe.whitelist()
def get_tasks(project):
	user_data = get_user_data()

	tasks = frappe.get_list("Task", filters={"project": project}, order_by="step_number ASC", fields=["*"])
	tasks_names = [x.name for x in tasks]

	fields = ["action_name", "customer_action", "supplier_action", "instructions"]

	actions = frappe.get_all(
		"Task Action",
		filters={"task": ("in", tasks_names), "status": "Started"},
		fields=["task"] + fields
	)

	for action in actions:
		action.update(TASK_ACTION_MAP.get(action.action_name))

	for task in tasks:
		task["display"] = True if task.status in ("Active", "Completed", "Suspended", "Late") else False
		if task["display"]:
			task["display"] = True if user_data.get("user_type") in ("Customer", "Admin") else task.supplier in user_data.get("suppliers", [])
		action = [x for x in actions if x.task == task.name]
		if action:
			task.update(action[0])

	return tasks

@frappe.whitelist()
def get_current_task(project):
	tasks = get_tasks(project)

	for task in tasks:
		if task.get("display") and task.get("action_name"):
			return task

	return {}

@frappe.whitelist()
def get_task_files(task):
	return frappe.get_all("File",
		filters={
			"attached_to_doctype": "Task",
			"attached_to_name": task,
			"file_status": "Original"
		},
		fields="*")

@frappe.whitelist()
def open_next_task(task):
	current_task = frappe.get_doc("Task", task)
	next_task = None

	if frappe.db.exists("Task", dict(project=current_task.project, step_number=current_task.step_number + 1)):
		next_task = frappe.get_doc("Task", dict(project=current_task.project, step_number=current_task.step_number + 1))

	current_task.close_task(next_task)

	if next_task:
		next_task.open_task()

def check_late_tasks():
	tasks = frappe.get_all("Task", filters={"status": "Active", "expected_end_date": ("<", nowdate())})
	for task in tasks:
		frappe.get_doc("Task", task.name).set_status("Late")