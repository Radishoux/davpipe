frappe.listview_settings['Task'] = {
	hide_name_column: true,
	get_indicator: function(doc) {
		let color = "darkgrey";
		if (doc.status == "Active") {
			color = "orange";
		} else if (doc.status == "Completed") {
			color = "green";
		} else if (doc.status == "Suspended") {
			color = "red";
		} else if (doc.status == "Not Started") {
			color = "gray";
		}

		return [__(doc.status), color];
	}
}