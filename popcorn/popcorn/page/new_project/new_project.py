import frappe
from frappe import _

@frappe.whitelist()
def get_project_with_file(project=None):
	if not project:
		return []

	project = frappe.get_doc("Project", project)

	if not project.original_document:
		return []

	uploaded_document = frappe.db.get_value("File", project.original_document, ["file_name", "file_url"], as_dict=True)

	output = project.as_dict()
	output["document"] = project.original_document
	output["file_name"] = uploaded_document.file_name
	output["file_url"] = uploaded_document.file_url

	return [output]

@frappe.whitelist()
def get_other_proof_files(project=None, status="Optional"):
	if not project:
		return []

	files = frappe.get_all("File", filters={
		"file_status": status,
		"attached_to_doctype": "Project",
		"attached_to_name": project
	}, fields=["name", "file_name", "file_url"])

	return files

@frappe.whitelist()
def get_id_card_files(project=None):
	return get_other_proof_files(project=project, status="ID Card")


@frappe.whitelist()
def get_power_of_attorney_files(project=None):
	return get_other_proof_files(project=project, status="Power of Attorney")
