frappe.provide("popcorn")
frappe.provide("popcorn.project")

frappe.pages['new-project'].on_page_show = function(wrapper) {
	$(wrapper).empty();

	frappe.ui.make_app_page({
		parent: wrapper,
		single_column: true
	});

	frappe.utils.set_title(__("New project"));

	const show_slides = (values, docname) => {
		const slidesSettings = {
			parent: wrapper,
			slides: popcorn_slides,
			slide_class: ProjectSlide,
			unidirectional: 1,
			done_state: 1,
			doc: { doctype: "Project", name: docname},
			before_load: ($footer) => {
				$footer.find('.text-right').prepend(
					$(`<button class="complete-btn btn btn-sm primary">${__("Submit")}</button>`));
			}
		}
		popcorn.project.values = values || {}
		popcorn.project.slides = new PopcornProject(slidesSettings)
	}

	let docname = null
	const route = frappe.router.get_sub_path_string().split('/')
	if (route.length === 2 && route[0] === "new-project") {
		docname = route[1]
	}

	if (docname) {
		return frappe.xcall('frappe.client.get', { doctype: "Project", name: docname })
		.then(r => {
			const {creation, modified, modified_by, ...values} = r;
			show_slides(values, docname)
		})
	} else {
		show_slides()
	}
}

class PopcornProject extends popcorn.ui.Slides {
	constructor(opts) {
		super(opts);
		Object.assign(this, opts)

		this.saving_in_progress = false
		this.submission_requested = false
		this.current_id = 0
	}

	make() {
		super.make()
		this.$next_btn.addClass('action');
		this.setup_keyboard_nav();
	}

	get_or_save_doc() {
		if (this.doc && this.doc.name) {
			return this.get_doc()
			.then(() => {
				return this.save_doc()
			})
		} else if (this.doc) {
			return this.save_doc()
		}
	}

	get_doc() {
		return frappe.xcall('frappe.client.get', { doctype: "Project", name: this.doc.name })
		.then(r => {
			this.doc = r;
			return this.doc;
		})
	}

	save_doc() {
		Object.assign(this.doc, this.values)
		this.saving_in_progress = true
		return frappe.xcall('frappe.client.save', { doc: this.doc })
		.then(r => {
			this.doc = r;
			this.saving_in_progress = false
			return this.doc;
		}).then(res => {
			if (this.submission_requested) {
				return this.submit_doc()
			} else {
				return res
			}
		})
	}

	submit_doc() {
		this.doc&&Object.assign(this.doc, this.values)
		frappe.xcall('frappe.client.submit', { doc: this.doc })
		.then(r => {
			frappe.set_route("Form", r.doctype, r.name)
		})
	}

	setup_keyboard_nav() {
		$('body').on('keydown', this.handle_enter_press.bind(this));
	}

	disable_keyboard_nav() {
		$('body').off('keydown', this.handle_enter_press.bind(this));
	}

	show_slide(id, previous) {
		if (id === this.slides.length) {
			this.action_on_complete();
			return;
		}

		!previous&&this.current_slide&&this.current_slide.set_values()
		super.update_values()

		if (id > 0) {
			this.get_or_save_doc().then(() => {
				super.show_slide(id);
			})
		} else {
			super.show_slide(id);
		}
	}

	action_on_complete() {
		if (!this.current_slide.set_values()) return;
		this.disable_keyboard_nav();
		this.$complete_btn.html(__("In progress..."))
		this.$complete_btn.attr("disabled", true)

		this.current_slide&&this.current_slide.set_values()
		super.update_values()

		if (this.saving_in_progress) {
			this.submission_requested = true
			return
		} else {
			return this.submit_doc()
		}
	}

	handle_enter_press(e) {
		if (e.which === frappe.ui.keyCode.ENTER) {
			var $target = $(e.target);
			if ($target.hasClass('prev-btn')) {
				$target.trigger('click');
			} else {
				this.container.find('.next-btn').trigger('click');
				e.preventDefault();
			}
		}
	}

	show_hide_prev_next(id) {
		if (!this.$complete_btn) {
			this.$complete_btn = this.$footer.find('.complete-btn').addClass('action');
		}
		super.show_hide_prev_next(id);

		if (id + 1 === this.slides.length) {
			this.$next_btn.removeClass("btn-primary").hide();
			this.$complete_btn.addClass("btn-primary").show()
				.on('click', this.action_on_complete.bind(this));
		} else if (id == 1 && (!popcorn.project.documents || popcorn.project.documents&&!popcorn.project.documents.files_uploaded)) {
			this.$next_btn.addClass("btn-primary").hide();
			this.$complete_btn.removeClass("btn-primary").hide();
		} else {
			this.$next_btn.addClass("btn-primary").show();
			this.$complete_btn.removeClass("btn-primary").hide();
		}

	}
};

class ProjectSlide extends popcorn.ui.Slide {
	constructor(slide = null) {
		super(slide);
	}

	make() {
		super.make();
		this.set_init_values();
		this.reset_action_button_state();
	}

	before_show() {
		this.after_setup&&this.after_setup(this)
	}

	set_init_values() {
		if (popcorn.project.values) {
			this.form.set_values(popcorn.project.values).then(() => {
				this.reset_action_button_state();
				this.after_setup&&this.after_setup(this)
			})
		}
	}

	reset_action_button_state() {
		var empty_fields = this.reqd_fields.filter((field) => {
			return !field.get_value();
		});

		if (empty_fields.length) {
			this.slides_footer.find('.action').attr('disabled', true);
		} else {
			this.slides_footer.find('.action').attr('disabled', false);
		}
	}

	hide_next_button() {
		this.slides_footer.find(".next-btn").hide()
	}

	show_next_button() {
		this.slides_footer.find(".next-btn").show()
	}

};



const popcorn_slides = [
	{
		name: 'base_information',
		title: __("Create your project"),
		icon: "fa fa-flag",
		fields: [
			{
				"fieldname": "customer",
				"label": __("Customer"),
				"fieldtype": "Link",
				"options": "Customer",
				"reqd": 1,
				"read_only": 1
			},
			{
				"fieldname": "project_name",
				"label": __("Project Name"),
				"fieldtype": "Data",
				"reqd": 1
			},
			{
				"fieldname": "country_of_destination",
				"label": __("Country of destination"),
				"reqd": 1,
				"fieldtype": "Link",
				"options": "Country"
			},
			{
				"fieldname": "issuing_country",
				"label": __("Issuing Country"),
				"fieldtype": "Link",
				"options": "Country",
				"hidden": 1,
				get_query: () => {
					return {
						filters: {
							name: ["!=", "France"]
						}
					};
				}
			}
		],

		onload: function(slide) {
			slide.form.set_input("customer", frappe.boot.user_data.customer.name)
			slide.form.get_field("project_name").set_invalid()
			slide.form.get_field("country_of_destination").set_invalid()
			slide.form.set_df_property("customer", "hidden", 1);

			slide.get_input("country_of_destination").unbind("change").on("change", function () {
				slide.slides.reset()
				const country = slide.get_field("country_of_destination").get_value()
				if (country == "France") {
					slide.form.set_df_property("issuing_country", "hidden", 0);
					slide.form.set_df_property("issuing_country", "reqd", 1);
				} else {
					slide.form.set_df_property("issuing_country", "hidden", 1);
					slide.form.set_df_property("issuing_country", "reqd", 0);
				}
			})
		},
		validate: function() {
			frappe.xcall("popcorn.popcorn.doctype.project.project.get_legalization_rules", {"country": this.get_value("country_of_destination")})
			.then(r => {
				popcorn.project.legalization_rules = r;
			})
			return true
		}
	},

	{
		name: 'signed_documents',
		title: __("Upload your documents"),
		icon: "fa fa-flag",
		fields: [
			{
				"fieldname": "signed_document",
				"label": __("Signed documents"),
				"fieldtype": "HTML",
				"description": __("PDF only")
			},
			{
				"fieldname": "type_of_document_example",
				"label": __("Type of document"),
				"fieldtype": "Select",
				"options": Object.keys(frappe.boot.type_of_documents_examples).map(e => {
					return {
						value: e,
						label: __(e)
					}
				}).sort(order),
				"reqd": 1,
				"hidden": 0
			},
			{
				"fieldname": "type_of_document",
				"label": __("Type of document"),
				"fieldtype": "Link",
				"options": "Type of document",
				"reqd": 1,
				"hidden": 1
			},
			{
				"fieldname": "type_of_document_html",
				"label": __("Type of documents HTML"),
				"fieldtype": "HTML"
			},
			{
				"fieldname": "legalization_question_1",
				"label": "",
				"fieldtype": "Select",
				"options": [{"label": __("Yes"), "value": "Yes"}, {"label": __("No"), "value": "No"}]
			},
			{
				"fieldname": "legalization_question_2",
				"label": "",
				"fieldtype": "Select",
				"options": [{"label": __("Yes"), "value": "Yes"}, {"label": __("No"), "value": "No"}]
			}
		],
		onload: function(slide) {
			slide.form.set_df_property("type_of_document_example", "hidden", 1);
			slide.form.set_df_property("legalization_question_1", "hidden", 1);
			slide.form.set_df_property("legalization_question_2", "hidden", 1);
			popcorn.project.documents = new SignedDocumentUploader(
				{
					field: "signed_document",
					slide: slide,
					method: "popcorn.popcorn.page.new_project.new_project.get_project_with_file",
					project: popcorn.project.slides.doc
				}
			)
		},
		after_setup() {
			const tod = this.get_value("type_of_document")
			tod&&show_type_of_document(this, tod)
			tod&&show_legalization_questions(this, tod)
		},
		validate: function() {
			return popcorn.project.documents.files_uploaded
		}
	},

	{
		name: 'more_information',
		title: __("Specify your project details"),
		icon: "fa fa-flag",
		fields: [
			{
				"fieldname": "type_of_legalization",
				"label": __("Type of legalization"),
				"fieldtype": "Select",
				"reqd": 1,
				"options": "Apostille\nLegalization\nExemption"
			},
			{
				"fieldname": "type_of_notarization",
				"label": __("Signature Notarization by"),
				"fieldtype": "Select",
				"reqd": 1,
				"options": "Gefi (CCI)\nNotary"
			},
			{
				"fieldname": "send_documents",
				"label": __("I want my documents sent once legalized"),
				"fieldtype": "Check"
			},
			{
				"fieldname": "delivery_address",
				"label": __("Delivery address"),
				"fieldtype": "HTML",
				"depends_on": "send_documents"
			},
			{
				"fieldname": "translate_doc",
				"label": __("My document needs to be translated"),
				"fieldtype": "Check"
			},
			{
				"fieldname": "certification_by_ccfa",
				"label": __("My document needs to be certified by CCFA"),
				"fieldtype": "Check",
				"hidden": 1,
				"description": __("What is this ? CCFA = Franco Arabic Chamber, only needed for documents to be sent to countries of the Arab League")
			}
		],
		onload: function(slide) {
			const me = this;
			this.display_address()

			slide.get_input("type_of_legalization").unbind("change").on("change", function () {
				me.set_exemption_rules(slide.get_field("type_of_legalization").get_value())
			});
		},
		display_address() {
			this.get_field("delivery_address").$wrapper.html(this.format_address())
			this.bind_address_btn()
		},
		format_address() {
			const add_display = frappe.boot.user_data.delivery_address_display
			const btn = add_display ? 
				`<button class="btn btn-default">${__("Edit address")}</button>` :
				`<button class="btn btn-default">${__("New address")}</button>`

			return popcorn.utils.display_address(__("Delivery address"), add_display, btn)
		},
		bind_address_btn() {
			const me = this
			this.get_field("delivery_address").$wrapper.find(".btn").on("click", function() {
				popcorn.utils.edit_address(
					"delivery_address",
					frappe.boot.user_data.delivery_address,
					frappe.boot.user_data.customer,
					(r) => {
						frappe.boot.user_data.delivery_address = r
						frappe.xcall('frappe.contacts.doctype.address.address.get_address_display',
							{ address_dict: r.name }).then(add => {
								frappe.boot.user_data.delivery_address_display = add
								me.display_address()
						})
					}
				)
			})
		},
		validate() {
			if (this.get_value("delivery_address") != 1 && !frappe.boot.user_data.delivery_address) {
				frappe.throw(__("Please register a delivery address"))
			} else {
				return true;
			}
		},
		after_setup() {
			if (popcorn.project.slides.doc.name) {
				frappe.db.get_value("Project", popcorn.project.slides.doc.name, ("type_of_legalization"), r => {
					this.form.fields_dict.type_of_legalization.set_input(r.type_of_legalization);
					this.form.fields_dict.type_of_legalization.$wrapper.trigger("input");
					this.set_exemption_rules(r.type_of_legalization)
				})

				frappe.db.get_value("Project", popcorn.project.slides.doc.name, ("certification_by_ccfa"), r => {
					this.form.fields_dict.certification_by_ccfa.set_input(r.certification_by_ccfa);
				})
			}
		},
		set_exemption_rules(type_of_legalization) {
			if (type_of_legalization == "Exemption") {
				this.form.set_df_property("type_of_notarization", "options", "None\nGefi (CCI)\nNotary");

				this.form.fields_dict.translate_doc.set_input(1);
				this.get_input("translate_doc").attr("disabled", true)
			} else {
				this.form.set_df_property("type_of_notarization", "options", "Gefi (CCI)\nNotary");
				this.form.fields_dict.type_of_notarization.set_input("Gefi (CCI)");

				this.get_input("translate_doc").attr("disabled", false)
			}
		}
	},

	{
		name: 'extra_information',
		title: __("Additional requirements"),
		subtitle: __("We advise you to add here any additional document you may be required to present"),
		icon: "fa fa-flag",
		fields: [
			{
				"fieldname": "id_documents",
				"fieldtype": "HTML",
			},
			{
				"fieldname": "power_of_attorney_documents",
				"fieldtype": "HTML",
			},
			{
				"fieldname": "other_proof_documents",
				"fieldtype": "HTML",
			},
			{
				"fieldname": "specific_comments",
				"label": __("Do you need to share any specific comment ?"),
				"fieldtype": "Small Text"
			}
		],
		onload: function(slide) {
			popcorn.project.id_documents = new popcorn.documents_list(
				{
					field: "id_documents",
					label: __("ID card of the signatory"),
					slide: slide,
					status: "ID Card",
					method: "popcorn.popcorn.page.new_project.new_project.get_id_card_files",
					project: popcorn.project.slides.doc
				}
			)

			popcorn.project.power_of_attorney_documents = new popcorn.documents_list(
				{
					field: "power_of_attorney_documents",
					label: __("Specific power of attorney for Davron Digital to proceed with any further additional legalization matter"),
					slide: slide,
					status: "Power of Attorney",
					method: "popcorn.popcorn.page.new_project.new_project.get_power_of_attorney_files",
					project: popcorn.project.slides.doc
				}
			)

			popcorn.project.other_documents = new popcorn.documents_list(
				{
					field: "other_proof_documents",
					label: __("Other documents"),
					slide: slide,
					status: "Optional",
					method: "popcorn.popcorn.page.new_project.new_project.get_other_proof_files",
					project: popcorn.project.slides.doc
				}
			)
		},
		validate: function() {
			promises = [popcorn.project.id_documents.upload_files(),
				popcorn.project.power_of_attorney_documents.upload_files(),
				popcorn.project.other_documents.upload_files()
			]

			return Promise.all(promises).then(() => {
				return true
			})
		}
	},
	{
		name: 'project_creation_recap',
		title: __("My Project"),
		icon: "fa fa-flag",
		fields: [
			{
				"fieldname": "project_recap",
				"fieldtype": "HTML",
			}
		],
		after_setup: function(slide) {
			frappe.model.with_doctype("Project", () => {
				const meta = frappe.get_meta("Project")
				const fields_dict = {}
				meta.fields.forEach(m => {
					fields_dict[m.fieldname] = {df: {}}
					fields_dict[m.fieldname].df = m
				})

				new popcorn.ui.ProjectFeed({
					frm: slide.form,
					tasks: [],
					fields_dict: fields_dict,
					parent: slide.get_field("project_recap").$wrapper,
					doc: popcorn.project.slides.doc,
					recap_only: true
				})
			})
		}
	}
];

const show_type_of_document = (slide, type_of_document) => {
	const document_definition = frappe.boot.type_of_documents[type_of_document]
	const legalization_type = $(`
		<div class="legalization-type mt-5 p-1">
			<div class="text-muted">
				${__("Type of document for legalization")}
				<span class="float-right flex">
					<div class="expand-button"><span>${__("Expand")} ${frappe.utils.icon("down", 'xs')}</span></div>
				</span>
			</div>
			<div class="control-label">
				${__(type_of_document)}
			</div>

			<div class="text-muted document-definition" style="display: None;">${document_definition || ""}</div>
		</div>
	`)

	legalization_type.find(".expand-button").on("click", () => {
		const legal = legalization_type.find(".document-definition").toggle()
		legalization_type.find(".expand-button").html($(legal).is(":visible") ?
			`<span>${__("Collapse")} ${frappe.utils.icon("up-line", 'xs')}</span>` :
			`<span>${__("Expand")} ${frappe.utils.icon("down", 'xs')}</span>`
		)
	})

	slide.get_field("type_of_document_html").$wrapper.html(legalization_type)
}

const show_legalization_questions = (slide, type_of_document) => {
	const obj = popcorn.project.legalization_rules.filter(f => {
		return f.type_of_document == type_of_document
	})

	if (obj.length && obj[0].case_values) {
		slide.form.set_df_property("legalization_question_1", "label", obj[0].case_values.legalization_question_text);
		slide.form.set_df_property("legalization_question_1", "reqd", 1);
		slide.form.set_df_property("legalization_question_1", "hidden", 0);
		slide.get_field("legalization_question_1").refresh()
	} else {
		slide.form.set_df_property("legalization_question_1", "reqd", 0);
		slide.form.set_df_property("legalization_question_1", "hidden", 1);
	}

	if (obj.length && obj[0].secondary_case_values) {
		slide.get_input("legalization_question_1").unbind("change").on("change", function () {
			if (slide.get_value("legalization_question_1") == "No") {
				slide.form.set_df_property("legalization_question_2", "label", obj[0].secondary_case_values.legalization_question_text);
				slide.form.set_df_property("legalization_question_2", "reqd", 1);
				slide.form.set_df_property("legalization_question_2", "hidden", 0);
				slide.get_field("legalization_question_2").refresh()
			} else {
				slide.form.set_df_property("legalization_question_2", "reqd", 0);
				slide.form.set_df_property("legalization_question_2", "hidden", 1);
			}
		});
	} else {
		slide.form.set_df_property("legalization_question_2", "reqd", 0);
		slide.form.set_df_property("legalization_question_2", "hidden", 1);
	}
}

function order(a, b) {
	return a.label < b.label ? -1 : (a.label > b.label ? 1 : 0);
}

class SignedDocumentUploader extends popcorn.documents_list {
	constructor(opts) {
		super(opts);
		this.removable = true
		Object.assign(this, opts)
		this.document_types = []
	}

	make() {
		this.get_data().then(() => {
			this.files.map(f => {
				return Object.assign(f, {
					type: "application/pdf",
					docname: f.name,
					name: f.file_name,
					progress: 1,
					total: 1
				})
			})

			if (!this.files_uploaded) {
				this.slide.hide_next_button();
			} else {
				this.unhide_slide_action();
			}

			this.show()
		})
	}

	unhide_slide_action() {
		this.slide.form.set_df_property("type_of_document_example", "hidden", 0);
		this.slide.show_next_button();
		this.bind_btns()
	}

	bind_btns() {
		const me = this;
		this.slide.get_input("type_of_document_example").unbind("change").on("change", function () {
			const example = me.slide.get_field("type_of_document_example").get_value()
			const type_of_document = frappe.boot.type_of_documents_examples[example]
			me.slide.form.fields_dict.type_of_document.set_input(type_of_document);

			show_type_of_document(me.slide, type_of_document)
			show_legalization_questions(me.slide, type_of_document)
		})
	}

	show() {
		const me = this;
		this.file_uploader = new popcorn.ui.FileUploader({
			wrapper: this.documents_field.$wrapper,
			doctype: this.project.doctype,
			docname: this.project.name,
			status: "Original",
			folder: `Home/${this.project.name}`,
			restrictions: {
				allowed_file_types: ["application/pdf"],
			},
			disable_file_browser: 1,
			allow_multiple: false,
			files: this.files,
			show_upload_button: !this.files.length,
			on_success(file_doc) {
				me.files_uploaded = true
				me.unhide_slide_action()
				me.upload_success(file_doc)
			},
			on_remove(index) {
				//
			}
		});
	}

	upload_success(file_doc) {
		frappe.xcall('frappe.client.set_value', {
			doctype: "Project",
			name: this.project.name,
			fieldname: "original_document",
			value: file_doc.name
		}).then(() => {
			this.files.push(file_doc);
		})
	}
}