frappe.pages['audit'].on_page_show = function(wrapper) {
	if (!popcorn.audit_log) {
		popcorn.audit_log = new popcorn_audit_log(wrapper);
	}
	popcorn.audit_log.show();
}

class popcorn_audit_log {
	constructor(wrapper) {
		this.page = frappe.ui.make_app_page({
			parent: wrapper,
			title: __("Audit Log"),
			single_column: true
		});

		this.wrapper = $(`<div></div>`)
		this.page.wrapper.find(".layout-main-section").html(this.wrapper)
		this.data = []

		this.docname = null
		const route = frappe.router.get_sub_path_string().split('/')
		if (route.length === 2 && route[0] === "audit") {
			this.docname = route[1]
		}

		if (this.docname) {
			this.build()
			this.page.set_secondary_action(__("Back to Project"), () => {
				frappe.set_route("Form", "Project", this.docname)
			})
		}
	}

	build() {
		this.get_data().then(r => {
			this.show()
		})
	}

	get_data() {
		return frappe.xcall('popcorn.popcorn.page.audit.audit.get_data', { name: this.docname })
		.then(r => {
			this.data = r
		})
	}

	show() {
		this.wrapper.empty();
		this.data.map(d => {
			const $card_body = d.value ? $(`
				<div class="row">
					<div class="col col-md-12 audit-card-body">
						<div class="row">
							<div class="col">
								<div class="mb-4">
									<div class="control-label value-section">${d.value}</div>
								</div>
							</div>
						</div>
					</div>
				</div>`) : ""

			return this.build_card(d, $card_body)
		})
	}

	build_card(data, card_body) {
		const link = data.link ? `
				<button class="btn btn-sm btn-info">${data.link_label}</button>
		` : ""

		this.$card = $(`
			<div class="project-feed-card frappe-card">
				<div class="project-card-head">
					<div class="flex">
						<div class="project-card-title flex">${__(data.log_type) || ""}</div>
						<div class="ml-auto action-button">${link}</div>
					</div>
					<div class="project-card-subtitle text-muted">${frappe.datetime.obj_to_user(data.creation) || ""}</div>
					<div class=${data.user ? "mb-3" : ""}>
						${data.user ? frappe.avatar(
							data.user,
							"avatar-xs"
						) : ""}
						</div>
					</div>
				<div class="audit-card-body">
				</div>
			</div>
		`)

		if (card_body) {
			const $el = this.$card.find(".audit-card-body")
			$el.html(card_body)
			$el.addClass("project-card-body mt-2")
		}

		if (data.link) {
			this.$card.find(".btn").on("click", () => {
				var w = window.open(frappe.urllib.get_full_url(data.link));
				if(!w) {
					frappe.msgprint(__("Please enable pop-ups")); return;
				}
			})
		}

		this.wrapper.append(this.$card)
	}
}