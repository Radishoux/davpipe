import os
import frappe
from frappe import _
from frappe.utils import getdate

log_map = {
	"Project Creation": "get_project_creation",
	"Project Modification": "get_doc_modification",
	"Project Submission": "get_project_submission",
	"Task Modification": "get_doc_modification",
	"Task Action Creation": "get_task_action_creation",
	"Task Action Modification": "get_doc_modification",
	"File Upload": "get_file_action",
	"File Download": "get_file_action",
	"Audit File Creation": "get_audit_file_creation",
	"Blockchain Registration": "blockchain_event",
	"Notification Sent": "notification_sent"
}

@frappe.whitelist()
def get_data(name):
	logs = frappe.get_all("Popcorn Audit Log",
		filters={"project": name, "log_type": ("!=", "Blockchain Call")},
		fields=["*"],
		order_by="creation DESC"
	)

	output = []
	for log in logs:
		method = log_map.get(log.log_type)
		if method:
			display = LogDisplay(name, log, logs)
			getattr(display, method)()
			if display.output:
				output.append(display.output)

	return output

class LogDisplay:
	def __init__(self, project, log, logs):
		self.project = project
		self.log = log
		self.logs = logs

		self.output = {
			"log_type": self.log.log_type,
			"creation": self.log.creation,
		}

	def get_project_creation(self):
		project = frappe.parse_json(self.log.value)
		self.output.update({
			"value": _("Project {0} ({1}) created").format(project.get("project_name"), self.log.project),
			"user": self.log.user
		})

	def get_task_creation(self):
		task = frappe.parse_json(self.log.value)
		self.output.update({
			"value": _("Task {0} ({1}) created").format(task.get("step"), task.get("name")),
			"user": self.log.user
		})

	def get_task_action_creation(self):
		task_action = frappe.parse_json(self.log.value)
		task_name = frappe.db.get_value("Task", task_action.task, "step")
		self.output.update({
			"log_type": _("Task {0}: Started").format(_(task_name)),
			"user": self.log.user
		})

	def get_audit_file_creation(self):
		self.output.update({
			"link": f"/api/method/popcorn.popcorn.page.audit.audit.get_log_file?project={self.project}&filename={self.log.audit_file_name}" or "",
			"link_label": _("Audit file") if self.log.audit_file_name else "",
		})

	def get_doc_modification(self):
		try:
			value = frappe.parse_json(self.log.value)
			if value.get("value"):
				fieldname = "project_name" if value.get("reference_doctype") == "Project" else "step"
				reference_doctype = "Task" if value.get("reference_doctype") == "Task Action" else value.get("reference_doctype")
				reference_name = frappe.db.get_value("Task Action", value.get("reference_docname"), "task") if value.get("reference_doctype") == "Task Action" else value.get("reference_docname")
				display_name = frappe.db.get_value(reference_doctype, reference_name, fieldname)
				if value.get("value") != "Late":
					self.output.update({
						"log_type": _("{0} {1}: {2}").format(
							_(reference_doctype) or "",
							_(display_name) or "",
							_(value.get("value")) or ""
						),
						"user": self.log.user
					})
				else:
					self.output = {}
			else:
				self.get_doc_comparison(value)
		except Exception:
			self.output = {}

	def get_project_submission(self):
		self.output.update({
			"value": _("Project submitted"),
			"user": self.log.user
		})

	def get_file_action(self):
		try:
			value = frappe.parse_json(self.log.value)
			filename = frappe.db.get_value(value.get("reference_doctype"), value.get("reference_docname"), "file_name") or ""
			self.output.update({
				"link": f"/api/method/popcorn.utils.file.download_file?filename={value.get('reference_docname')}",
				"link_label": _("Download file"),
				"value": _("File {0} uploaded").format(filename) if self.log.log_type == "File Upload" else _("File {0} downloaded").format(filename),
				"user": self.log.user
			})
		except Exception:
		# For backwards compatibility
			self.output.update({
				"value": self.log.value,
				"user": self.log.user
			})

	def get_file_metadata(self):
		log = frappe.parse_json(self.log.value)
		metadata = log if isinstance(log, list) else frappe.parse_json(log.get("value"))
		if metadata:
			metaobject = metadata[0]
			issuer = metaobject.get("certificateInfo", {}).get("issuerOIDs", {}).get("O")
			validity = getdate(metaobject.get("certificateInfo", {}).get("notValidBefore")) <= getdate() <= getdate(metaobject.get("certificateInfo", {}).get("notValidAfter"))

			filename = ""
			if isinstance(log, dict):
				self.output.update({
					"link": f"/api/method/popcorn.utils.file.download_file?filename={log.get('reference_docname')}",
					"link_label": _("Download file")
				})

				filename = f'<div class="mb-4"><div class="text-muted">{_("Filename")}</div><div class="control-label">{log.get("reference_docname")}</div></div>'

			self.output.update({
				"value": f'<div class="col col-md-12">'
							f"{filename}"
							f'<div class="mb-4">'
								f'<div class="text-muted">{_("Signature")}</div>'
								f'<div class="control-label">{_("Yes") if self.log.value else __("No")}</div>'
							f'</div>'
							f'<div class="mb-4">'
								f'<div class="text-muted">{_("Certificate")}</div>'
								f'<div class="control-label">{issuer or ""}</div>'
							f'</div>'
							f'<div class="mb-4">'
								f'<div class="text-muted">{_("Valid")}</div>'
								f'<div class="control-label">{_("Yes") if validity else _("No")}</div>'
							f'</div>'
						f'</div>'
			})
		else:
			self.output = {}

	def blockchain_event(self):
		if self.log.log_type == "Blockchain Registration":
			if self.log.sent_to_tezos:
				job = frappe.get_doc("Tezos DigiSign Job", self.log.sent_to_tezos)
				if job.transaction_hash or job.block_hash:
					self.output.update({
						"value": f'<div class="col col-md-12">'
								f'<div class="mb-4">'
									f'<div class="text-muted">{_("Transaction")}</div>'
									f'<div class="control-label">{job.transaction_hash or _("Not available")}</div>'
								f'</div>'
								f'<div class="mb-4">'
									f'<div class="text-muted">{_("Block")}</div>'
									f'<div class="control-label">{job.block_hash or _("Not available")}</div>'
								f'</div>'
							f'</div>'
					})
				else:
					self.output = {}

	def notification_sent(self):
		pass

	def get_doc_comparison(self, doc):
		similar_logs = []

		for x in self.logs:
			try:
				value = frappe.parse_json(x.get("value"))
				if value.get("name") == doc.get("name"):
					similar_logs.append(value)
			except Exception:
				continue

		if similar_logs and len(similar_logs) > 1:
			distinct_items = {k: doc[k] for k in doc if k in similar_logs[1] and doc[k] != similar_logs[1][k]}
			if distinct_items:
				self.output.update({
					"value": f"Values modified:\n{frappe.as_json(distinct_items, indent=4)}"
				})

		if "value" not in self.output:
			self.output = {}


@frappe.whitelist()
def get_log_file(project, filename):
	path = frappe.get_site_path("private", "files", "logs", project)

	content = ""
	with open(os.path.join(path, filename)) as f:
		content = f.read()

	if content:
		frappe.local.response.filename = filename
		frappe.local.response.filecontent = content
		frappe.local.response.type = "download"