import frappe
from frappe import _
from frappe.contacts.doctype.address.address import get_preferred_address, get_address_display

@frappe.whitelist()
def get_user_data():
	username = frappe.session.user

	customer = {}
	suppliers= []
	supplier_types = []
	billing_address = {}
	delivery_address = {}

	customers = frappe.get_all("Customer Users", filters={"user": username}, fields=["parent"])
	if customers:
		customer = frappe.get_doc("Customer", customers[0]["parent"])

		billing_address = get_preferred_address(customer.doctype, customer.name, "is_primary_address") if customer else None
		delivery_address = get_preferred_address(customer.doctype, customer.name, "is_shipping_address") if customer else None

	else:
		suppliers = frappe.get_all("Supplier Users", filters={"user": username}, pluck="parent")
		if not frappe.flags.in_migrate and not suppliers and not "System Manager" in frappe.get_roles():
			frappe.throw(_("Your user does not have enough permissions to continue. Please contact the Davron Digital team."))

		supplier_types = frappe.get_all("Supplier", filters={"name": ("in", suppliers)}, pluck="supplier_type")

	return {
		"user": frappe.get_doc("User", username),
		"customer": customer,
		"suppliers": suppliers,
		"user_type": "Admin" if "System Manager" in frappe.get_roles() else ("Customer" if customer else "Supplier"),
		"supplier_types": supplier_types,
		"billing_address": frappe.get_doc("Address", billing_address) if billing_address else {},
		"billing_address_display": get_address_display(billing_address) if billing_address else "",
		"delivery_address": frappe.get_doc("Address", delivery_address) if delivery_address else {},
		"delivery_address_display": get_address_display(delivery_address) if delivery_address else ""
	}

@frappe.whitelist()
def register_address(doc):
	doc = frappe.parse_json(doc)
	if doc.get("name"):
		frappe.db.set_value("Address", doc.get("name"), "disabled", 1)
		doc["name"] = None

	address = frappe.get_doc(doc)
	return address.insert(ignore_permissions=True)

@frappe.whitelist()
def update_user_image(user, image):
	return frappe.db.set_value("User", user, "user_image", image)

@frappe.whitelist()
def update_lang(user, lang):
	frappe.db.set_value("User", user, "language", lang)
	frappe.cache().hdel("lang", user)
	return frappe.set_user_lang(user, lang)