frappe.provide("popcorn.profile");

frappe.pages["popcorn-profile"].on_page_load = function (wrapper) {
	popcorn.profile = new popcorn_profile(wrapper);
	popcorn.profile.show();
};

class popcorn_profile {
	constructor(wrapper) {
		this.page = frappe.ui.make_app_page({
			parent: wrapper,
			single_column: true,
		});

		this.sidebar = this.page.wrapper.find(".layout-side-section");
		this.main_section = this.page.wrapper.find(".layout-main-section");
		this.user = {};
		this.customer = {};
		this.billing_address = {};
		this.delivery_address = {};
		this.billing_address_display = "";
		this.delivery_address_display = "";

		this.image_section = this.page.wrapper.find(".sidebar-image-section");
		this.image_actions = this.page.wrapper.find(".sidebar-image-actions");
	}

	show() {
		this.page.wrapper
			.find(".page-body")
			.append(frappe.render_template("popcorn_profile"));
		this.set_profile_picture()
		document.getElementById("full-name").innerHTML = frappe.user.full_name();

		if (frappe.user.is_report_manager()) {
			document.getElementById("general_information").className = "col-12";
			document.getElementById("adress-div").style.display = "none";
			document.getElementById("adress-div").className = "";
		}
		frappe
			.xcall(
				"popcorn.popcorn.page.popcorn_profile.popcorn_profile.get_user_data"
			)
			.then((r) => {
				this.user = r.user;
				this.customer = r.customer;
				this.billing_address = r.billing_address;
				this.billing_address_display = r.billing_address_display;
				this.delivery_address = r.delivery_address;
				this.delivery_address_display = r.delivery_address_display;
				this.phone_number = r.user.phone;
				this.vat_number = r.customer.vat_number;
				this.language = r.user.language;
				this.create_fields();
				this.bind_profile_picture()
			});

		this.page.add_action_item(__("Reset Password"), function () {
			frappe.call({
				method: "frappe.core.doctype.user.user.reset_password",
				args: {
					user: frappe.session.user,
				},
			});
		});
	}

	create_fields() {
		this.create_phone_field();
		this.create_email_field();
		this.create_customer_field();
		this.create_vat_number_field();
		this.create_language_field();
		this.create_billing_address_field();
		this.create_delivery_address_field();
	}

	create_phone_field() {
		const me = this;
		this.phone_number_field = frappe.ui.form.make_control({
			parent: $("#phone_number"),
			df: {
				label: __("Phone Number"),
				fieldname: "phone_number",
				fieldtype: "Data",
				placeholder: __("Phone Number"),
				onchange: function () {
					if (this.value != "" && this.value != me.phone_number) {
						me.phone_number = this.value;
						frappe.db.set_value("User", me.user.name, "phone", me.phone_number)
						frappe.show_alert({
							indicator: "green",
							message: __("Phone number updated")
						})
					}
				},
			},
			render_input: true,
		});
		this.phone_number_field.set_value(this.user.phone);
	}

	create_email_field() {
		this.email_address_field = frappe.ui.form.make_control({
			parent: $("#email_address"),
			df: {
				label: __("Email address"),
				fieldname: "email_address",
				fieldtype: "Read Only",
				placeholder: __("Email address"),
			},
			render_input: true,
		});
		this.email_address_field.set_value(this.user.name);
	}

	create_customer_field() {
		this.customer_field = frappe.ui.form.make_control({
			parent: $("#customer"),
			df: {
				label: __("Customer"),
				fieldname: "customer",
				fieldtype: "Read Only",
				placeholder: __("Customer"),
			},
			render_input: true,
		});
		this.customer_field.set_value(this.customer.customer_name);
	}

	create_vat_number_field() {
		const me = this;
		this.vat_number_field = frappe.ui.form.make_control({
			parent: $("#vat_number"),
			df: {
				label: __("VAT Number"),
				fieldname: "vat_number",
				fieldtype: "Data",
				placeholder: __("VAT Number"),
				onchange: function () {
					if (this.value != "" && this.value != me.vat_number) {
						me.vat_number = this.value;
						frappe.db.set_value("Customer", me.customer.name, "vat_number", me.vat_number)
						frappe.show_alert({
							indicator: "green",
							message: __("VAT Number updated")
						})
					}
				},
			},
			render_input: true,
		});
		this.vat_number_field.set_value(this.customer.vat_number);
	}

	create_language_field() {
		const me = this;
		this.language_field = frappe.ui.form.make_control({
			parent: $("#language"),
			df: {
				label: __("Language"),
				fieldname: "language",
				fieldtype: "Select",
				default: "fr",
				options: [
					{ label: __("French"), value: "fr" },
					{ label: __("English"), value: "en" }
				],
				onchange: function () {
					if (this.value != me.language) {
						me.language = this.value;
						frappe.xcall("popcorn.popcorn.page.popcorn_profile.popcorn_profile.update_lang", {
							user: frappe.session.user,
							lang: this.value
						}).then(() => {
							frappe.show_alert({
								indicator: "green",
								message: __("Language updated")
							})
							frappe.ui.toolbar.clear_cache();
						})
					}
				},
			},
			render_input: true,
		});
		this.language_field.set_value(this.user.language);
	}


	create_billing_address_field() {
		const me = this;
		this.billing_address_field = frappe.ui.form.make_control({
			parent: $("#billing_address"),
			df: {
				label: __("Billing Address"),
				fieldname: "billing_address",
				fieldtype: "HTML",
				placeholder: __("Billing Address"),
			},
			render_input: true,
		});

		const address_diplay = this.display_addresses(
			__("Billing Address"),
			this.billing_address_display
		);
		this.billing_address_field.set_value(address_diplay);
		this.billing_address_field.$wrapper.find(".btn").on("click", function () {
			popcorn.utils.edit_address(
				"billing_address",
				me.billing_address,
				me.customer,
				(r) => {
					me.get_address_display(r);
				}
			);
		});
	}

	create_delivery_address_field() {
		const me = this;
		this.delivery_address_field = frappe.ui.form.make_control({
			parent: $("#delivery_address"),
			df: {
				label: __("Delivery Address"),
				fieldname: "delivery_address",
				fieldtype: "HTML",
				placeholder: __("Delivery Address"),
			},
			render_input: true,
		});

		const address_diplay = this.display_addresses(
			__("Delivery Address"),
			this.delivery_address_display
		);
		this.delivery_address_field.set_value(address_diplay);

		this.delivery_address_field.$wrapper.find(".btn").on("click", function () {
			popcorn.utils.edit_address(
				"delivery_address",
				me.delivery_address,
				me.customer,
				(r) => {
					me.get_address_display(r);
				}
			);
		});
	}

	display_addresses(title, add_display) {
		const btn = add_display
			? `<button class="btn btn-default">${__("Edit address")}</button>`
			: `<button class="btn btn-default">${__("New address")}</button>`;
		return popcorn.utils.display_address(title, add_display, btn);
	}

	get_address_display(address) {
		frappe
			.xcall("frappe.contacts.doctype.address.address.get_address_display", {
				address_dict: address.name,
			})
			.then((r) => {
				if (address.is_primary_address) {
					this.billing_address_display = r;
					const add_display = this.display_addresses(
						__("Billing address"),
						this.billing_address_display
					);
					this.billing_address_field.set_value(add_display);
				} else {
					this.delivery_address_display = r;
					const add_display = this.display_addresses(
						__("Delivery address"),
						this.delivery_address_display
					);
					this.delivery_address_field.set_value(add_display);
				}
			});
	}

	set_profile_picture() {
		document.getElementById("avatar").innerHTML = frappe.avatar(
			frappe.session.user,
			"avatar-large"
		);
	}

	bind_profile_picture() {
		const me = this;
		$("#avatar").on("click", () => {
			new frappe.ui.FileUploader({
				doctype: "User",
				docname: this.user.name,
				on_success(file_doc) {
					frappe.xcall("popcorn.popcorn.page.popcorn_profile.popcorn_profile.update_user_image", {
						user: me.user.name,
						image: file_doc.file_url
					}).then(() => {
						frappe.ui.toolbar.clear_cache();
					})
				}
			})
		})
	}
}
