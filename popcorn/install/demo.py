import frappe


def create_suppliers():
	suppliers = [
		{
			"supplier_name": "Davron Translations",
			"supplier_type": "Translator",
			"delivery_timeframe": 5,
			"country": None,
			"doctype": "Supplier",
			"supplier_users": [
				{
					"user": "testsupplier@davrontranslations.com"
				}
			]
		},
		{
			"supplier_name": "Court of Appeals",
			"supplier_type": "Court of Appeals",
			"delivery_timeframe": 2,
			"country": None,
			"doctype": "Supplier",
			"supplier_users": [
				{
					"user": "testsupplier@davrontranslations.com"
				}
			]
		},
		{
			"supplier_name": "CCFA",
			"supplier_type": "Franco Arabic Chamber of Commerce",
			"delivery_timeframe": 2,
			"country": None,
			"doctype": "Supplier",
			"supplier_users": [
				{
					"user": "testsupplier@davrontranslations.com"
				}
			]
		},
		{
			"supplier_name": "Ministère des affaires étrangères",
			"supplier_type": "Ministry of Foreign Affairs",
			"delivery_timeframe": 2,
			"country": None,
			"doctype": "Supplier",
			"supplier_users": [
				{
					"user": "testsupplier@davrontranslations.com"
				}
			]
		},
		{
			"supplier_name": "Chambre de commerce et d'industrie",
			"supplier_type": "Chamber of Commerce",
			"delivery_timeframe": 1,
			"country": None,
			"doctype": "Supplier",
			"supplier_users": [
				{
					"user": "testsupplier@davrontranslations.com"
				}
			]
		},
		{
			"supplier_name": "Notaire",
			"supplier_type": "Notary",
			"delivery_timeframe": 1,
			"country": None,
			"doctype": "Supplier",
			"supplier_users": [
				{
					"user": "testsupplier@davrontranslations.com"
				}
			]
		},
		{
			"supplier_name": "Ordre des médecins",
			"supplier_type": "French Medical Association",
			"delivery_timeframe": 2,
			"country": None,
			"doctype": "Supplier",
			"supplier_users": [
				{
					"user": "testsupplier@davrontranslations.com"
				}
			]
		}
	]

	for supplier in suppliers:
		try:
			frappe.get_doc(supplier).insert(ignore_permissions=True)
		except frappe.DuplicateEntryError:
			pass

	for supplier in frappe.get_all("Supplier", filters=dict(country=("is", "set")), pluck="name"):
		doc = frappe.get_doc("Supplier", supplier)
		doc.append("supplier_users", {
			"user": "testsupplier@davrontranslations.com"
		})
		doc.save()

def create_customer():
	frappe.get_doc({
		"doctype": "Customer",
		"customer_name": "Test Customer",
		"users": [
			{
				"user": "testclient@davrontranslations.com"
			}
		]
	}).insert()

def create_users():
	from frappe.utils.password import update_password
	users = [
		{
			"doctype": "User",
			"email": "testclient@davrontranslations.com",
			"first_name": "Test",
			"last_name": "Client",
			"send_welcome_email": 0
		},
		{
			"doctype": "User",
			"email": "testsupplier@davrontranslations.com",
			"first_name": "Test",
			"last_name": "Supplier",
			"send_welcome_email": 0
		}
	]

	for user in users:
		try:
			doc = frappe.get_doc(user).insert(ignore_permissions=True)
			update_password(doc.name, "c8J3vQl21Hs3")
		except frappe.DuplicateEntryError:
			pass

def generate_demo_data():
	create_users()
	create_suppliers()
	create_customer()