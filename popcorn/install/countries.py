import frappe
from frappe import _

ARAB_LEAGUE = ["Algeria", "Saudi Arabia", "Palestinian Territory", "Bahrain", "Comoros", "Djibouti", "Egypt", "United Arab Emirates", "Irak", "Jordania", "Kuwait", "Lebanon", "Libya", "Morocco", "Mauritania", "Oman", "Qatar", "Somalia", "Sudan", "Tunisia", "Yemen"]

def create_consulates():
	for country in frappe.get_all("Legalization Rule",
		filters=dict(type_of_legalization=("in", ("Legalization", "Apostille", "Exception"))),
		distinct=1,
		pluck="country"
	):
		try:
			frappe.get_doc({
				"supplier_name": f"Consulate of {country}",
				"supplier_type": "Consulate",
				"delivery_timeframe": 15,
				"country": country,
				"doctype": "Supplier"
			}).insert(ignore_permissions=True)
		except frappe.DuplicateEntryError:
			pass

def get_translated_countries(filters=None):
	from babel.dates import Locale
	from collections import Counter

	translated_dict = {}
	locale = Locale.parse(frappe.local.lang, sep="-")

	country_list = frappe.get_all("Country", fields=["name", "code"], filters=filters)
	country_codes = [x.code for x in country_list]
	duplicate_codes = [item for item, count in Counter(country_codes).items() if count > 1 and item is not None]

	for country in country_list:
		if country.code and country.code not in duplicate_codes:
			translated_dict[country.name] = locale.territories.get(country.code.upper())
		else:
			translated_dict[country.name] = _(country.name)

	return translated_dict