from frappe import _

REJECTION_REASONS = [
	{
		"rejection_reason": "A document is missing",
		"placeholder": "The ID of the signatory is missing. Please send us this proof document to resume the project.",
	},
	{
		"rejection_reason": "A signature is missing",
		"placeholder": "The document is missing a signature. Please sign the document and send it back to us.",
	},
	{
		"rejection_reason": "Some information is missing",
		"placeholder": "Some information is missing. Please send us the missing information to resume the project.",
	},
	{
		"rejection_reason": "The Document is illegible",
		"placeholder": "The Document is illegible. Please send us a readable document.",
	},
	{
		"rejection_reason": "Closed institution",
		"placeholder": "The institution will not proceed with any legalization until [date].",
	}
]