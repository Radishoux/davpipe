import frappe
import os
from os import remove, fdopen
from shutil import move
from tempfile import mkstemp

def replace(file_path, pattern, subst):
	#Create temp file
	fh, abs_path = mkstemp()
	with fdopen(fh,'w') as new_file:
		with open(file_path) as old_file:
			for line in old_file:
				new_file.write(line.replace(pattern, subst))
	#Remove original file
	remove(file_path)
	#Move new file
	move(abs_path, file_path)

def override_desktop_bug():
	frappe_file = frappe.get_app_path("frappe", "desk", "desktop.py")

	pattern = "if self.is_item_allowed(item.get('name'), item.get('type')):"
	subst = "if self.is_item_allowed(item.get('link_to'), item.get('link_type')):"
	try:
		replace(frappe_file, pattern, subst)
	except Exception as e:
		print(e)

def execute():
	override_desktop_bug()