import frappe

def execute():
	frappe.flags.in_patch = True
	navbar_settings = frappe.get_single("Navbar Settings")

	for navbar_item in navbar_settings.settings_dropdown:
		for route in ["/app/user-profile", "/app/background_jobs"]:
			if navbar_item.get("route") == route:
				navbar_settings.remove(navbar_item)


		for action in ["frappe.ui.toolbar.route_to_user()", "frappe.ui.toolbar.setup_session_defaults()", "frappe.ui.toolbar.view_website()"]:
			if navbar_item.get("action") == action:
				navbar_settings.remove(navbar_item)

	if not frappe.db.exists('Navbar Item', {'route': '/app/popcorn-profile', 'item_label': 'User Profile'}):
		for navbar_item in navbar_settings.settings_dropdown:
			navbar_item.idx = navbar_item.idx + 1

		navbar_settings.append('settings_dropdown', {
			'item_label': 'User Profile',
			'item_type': 'Route',
			'route': '/app/popcorn-profile',
			'is_standard': 1,
			'idx': 1
		})

	navbar_settings.save()
	frappe.db.commit()
	frappe.flags.in_patch = False
