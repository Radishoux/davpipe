from frappe import _

QUESTIONS = [
	{
		"question": _("Is the document a document drawn up by an administration relating to a commercial or customs operation?"),
		"question_code": "ADM-COMM"
	},
	{
		"question": _("Is the document a public deed relating to birth, life, death, name, marriage (including capacity to marry and marital status) etc. or a public deed/private deed with a material certification of signature produced in the course of legal proceedings relating to civil and commercial matters, excluding decisions relating to bankruptcy, composition and judicial settlement?"),
		"question_code": "PUB-WITH-CERT"
	},
	{
		"question": _("Is the document a public deed relating to birth, life, death, name, marriage (including capacity to marry and marital status) etc.?"),
		"question_code": "PUB-WITHOUT-CERT"
	},
	{
		"question": _("Is the document a judicial or extra-judicial document produced in the framework of mutual legal assistance procedures (cf. (a) bilateral)?"),
		"question_code": "JUDICIAL"
	},
	{
		"question": _("Is the document a public deed relating to birth, life, death, name, marriage (including capacity to marry and marital status) etc. or a document relating to the protection of minors?"),
		"question_code": "PUB-MINOR"
	},
	{
		"question": _("Is the document a deed relating to the civil status, capacity or family situation of natural persons, their nationality, domicile and residence, and all other deeds and documents where these are produced with a view to officiating a marriage or establishing a civil status record?"),
		"question_code": "DEED-STATUS"
	}
]

EXCEPTIONS = [
	{
		"case_name": "CASE 1",
		"legalization_question": "ADM-COMM",
		"yes_field": "Legalization",
		"no_field": "Apostille"
	},
	{
		"case_name": "CASE 2",
		"legalization_question": "PUB-WITH-CERT",
		"yes_field": "Exemption",
		"no_field": "Apostille"
	},
	{
		"case_name": "CASE 4",
		"legalization_question": "PUB-WITHOUT-CERT",
		"yes_field": "Exemption",
		"no_field": "Apostille"
	},
	{
		"case_name": "CASE 5",
		"legalization_question": "JUDICIAL",
		"yes_field": "Exemption",
		"no_field": "Legalization"
	},
	{
		"case_name": "CASE 6",
		"legalization_question": "PUB-MINOR",
		"yes_field": "Exemption",
		"no_field": "Apostille"
	},
	{
		"case_name": "CASE 7",
		"legalization_question": "DEED-STATUS",
		"yes_field": "Exemption",
		"no_field": "Apostille"
	},
	{
		"case_name": "CASE 10",
		"legalization_question": "PUB-MINOR",
		"yes_field": "Exemption",
		"no_field": "Apostille"
	},
	{
		"case_name": "CASE 3",
		"legalization_question": "ADM-COMM",
		"yes_field": "Legalization",
		"no_field": "Question",
		"secondary_case": "CASE 4",
	},
	{
		"case_name": "CASE 8",
		"legalization_question": "ADM-COMM",
		"yes_field": "Legalization",
		"no_field": "Question",
		"secondary_case": "CASE 7",
	},
	{
		"case_name": "CASE 9",
		"legalization_question": "ADM-COMM",
		"yes_field": "Legalization",
		"no_field": "Question",
		"secondary_case": "CASE 10",
	}
]