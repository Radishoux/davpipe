from frappe import _

TYPES_OF_DOCUMENTS = [
	{
		"examples": [_("Birth certificate"), _("Marriage certificate"), _("Death certificate")],
		"document_type": _("Civil status certificate"),
		"definition_en": "Certificate drawn up by a civil registrar on a civil status register (town hall or French Consulate). Must bear the seal that includes the Marianne.",
		"definition_fr": "Acte établi par un officier de l’état civil sur un registre de l’état civil (mairie ou Consulat de France). Doit comporter le sceau avec Marianne."
	},
	{
		"examples": [_("Company registration certificate (Kbis)"), _("Ruling (e.g. divorce)"), _("Arbitral award")],
		"document_type": _("Judicial deed"),
		"definition_en": "Documents issued by the administration service of a court. It must bear the seal of the court’s administration service.",
		"definition_fr": "Documents émis par le Greffe d’un tribunal. Il doit comporter le sceau du greffe."
	},
	{
		"examples": [_("Sworn affidavit of birth, if the birth was not registered in time"), _("Affidavit or written statement registered in a court of law")],
		"document_type": _("Affidavit filed in court"),
		"definition_en": "Document filed in a court of law.  \nA sworn statement, or affidavit, is a written document in which one solemnly declares before a person authorized by law, such as a notary public, that the information contained therein is true.",
		"definition_fr": "Document déposé dans un tribunal judiciaire.  \nUne déclaration sous serment, ou affidavit, est un écrit par lequel on déclare solennellement devant une personne autorisée par la loi, par exemple un notaire public, que les faits qui y sont énoncés sont vrais."
	},
	{
		"examples": [_("Deed/promise of sale of a property"), _("Inheritance certificate"), _("Will"), _("Copy in minutes or as a patent"), _("Authentic deed")],
		"document_type": _("Notarial deed"),
		"definition_en": "Deed issued by a notary. It must bear the notarial seal.",
		"definition_fr": "Acte émis par un notaire. Il doit comporter le sceau notarial."
	},
	{
		"examples": [_("Diploma"), _("Criminal record"), _("Nationality certificate"), _("Passport")],
		"document_type": _("Administrative deed"),
		"definition_en": "Established by a French administrative authority",
		"definition_fr": "Établis par une autorité administrative française"
	},
	{
		"examples": [_("Annuitant's life certificate")],
		"document_type": _("Annuitant's life certificate"),
		"definition_en": 'Life certificate issued by a person receiving a life annuity in order to prove to the person issuing the annuity that he or she is still alive: may take the form of an individual civil status form bearing the words "not deceased".',
		"definition_fr": 'Certificat de vie émis par une personne recevant une rente viagère afin de prouver à la personne délivrant la rente qu’elle est toujours en vie : peut se présenter sous la forme d’une fiche individuelle d’état civil portant la mention « non décédé ».'
	},
	{
		"examples": [_("Certificate of registration of a trade mark")],
		"document_type": _("INPI certificate"),
		"definition_en": "[Copy of an original administrative document from the National Register of Trademarks bearing the official stamp of the INPI (National Institute of Industrial Property) certifying its conformity](https://www.inpi.fr/fr/certificat-d-identite-ou-copie-officielle-d-une-marque-francaise)",
		"definition_fr": "[Copie d’un document administratif original, issue du Registre national des marques et revêtue du cachet officiel de l'INPI (Institut national de la propriété intellectuelle) attestant de sa conformité](https://www.inpi.fr/fr/certificat-d-identite-ou-copie-officielle-d-une-marque-francaise)"
	},
	{
		"examples": [_("Entry on the register of French nationals living abroad"), _("Registration on the electoral roll"), _("Passes in the event of theft or loss of IP")],
		"document_type": _("Document drawn up by consular officer"),
		"definition_en": "[Document issued by a consular officer working for the French consulate in another country](https://uk.ambafrance.org/-Vos-formalites-)",
		"definition_fr": "[Document émis par un agent consulaire travaillant pour le consulat français dans un autre pays](https://uk.ambafrance.org/-Vos-formalites-)"
	},
	{
		"examples": [_("Acknowledgement of debt"), _("Letter of recommendation"), _("Sworn statement"), _("Accommodation certificate"), _("Articles of Association"), _("Commercial documents"), _("Shareholder's decision"), _("Medical certificate"), _("Power of attorney"), _("Certificate of Free Sale")],
		"document_type": _("Private deed (only with certification of signature)"),
		"definition_en": 'On which an official statement is affixed - commercial document  \n"E.g.: free sales agreement"  \nThe "private deed" (or private signature) is a written agreement drawn up by the parties themselves or by a third party, which has been signed by them or by a person they have appointed as their representative with a view to settling a contractual situation (sale, rental, company, employment contract, etc.) without the presence of a public official  \nFor a medical certificate, obtain notarization by the medical association  \nCertification of signature by a notary is required.  \n[https://www.demarches.interieur.gouv.fr/particuliers/legalisation-documents-origine-etrangere-authentification](https://www.demarches.interieur.gouv.fr/particuliers/legalisation-documents-origine-etrangere-authentification)',
		"definition_fr": "Sur lequel une mention officielle est apposée – document commercial  \n« Ex : cvl (contrat de vente libre) »  \nL'acte « sous seing privé » (ou sous signature privée) est une convention écrite établie par les parties elles-mêmes ou par un tiers, qui a été signée par elles ou par une personne qu'elles ont constituée pour mandataire en vue de régler une situation contractuelle (vente, location, société, contrat de travail...) sans présence d’un officier public  \nPour certificat médical, obtenir la notarisation par l’ordre des médecins  \nCertification de signature par un notaire nécessaire.  \n[https://www.demarches.interieur.gouv.fr/particuliers/legalisation-documents-origine-etrangere-authentification](https://www.demarches.interieur.gouv.fr/particuliers/legalisation-documents-origine-etrangere-authentification)"
	}
]