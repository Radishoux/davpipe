import json
import os

import frappe
from frappe import _
from popcorn.install.document_types import TYPES_OF_DOCUMENTS
from popcorn.install.rejection_reasons import REJECTION_REASONS
from popcorn.install.legalization_questions import QUESTIONS, EXCEPTIONS
from popcorn.install.countries import create_consulates
from popcorn.install.after_migrate import execute

def initialize_system():
	from frappe.desk.page.setup_wizard.setup_wizard import setup_complete

	completed = setup_complete({
		"full_name": "Anne-Cécile Davron",
		"email": "ac@davrontranslations.com",
		"password": "c8J3vQl21Hs3",
		"currency": 'EUR',
		"timezone": 'Europe/Paris',
		"country": 'France',
		"language": "English",
		"lang": "en"
	})
	print("Initialization completed", completed)

def add_countries():
	with open(os.path.join(os.path.dirname(__file__), "country_info.json"), "r") as local_info:
		countries = json.loads(local_info.read())

	for country in countries:
		erpnext_name = countries[country].get("erpnext_name")
		if erpnext_name != country and frappe.db.exists("Country", countries[country].get("erpnext_name")):
			frappe.rename_doc("Country", erpnext_name, country)

		if not frappe.db.exists("Country", country):
			frappe.get_doc({
				"doctype": "Country",
				"country_name": country
			}).insert(ignore_permissions=True)

	full_list = frappe.get_all("Country", pluck="name")
	file_list = [x for x in countries]

	for data in full_list:
		if data not in file_list:
			frappe.delete_doc("Country", data)

def create_french_address_template():
	frappe.get_doc({
		"doctype": "Address Template",
		"country": "France",
		"is_default": 1,
	}).insert(ignore_permissions=True)

def create_types_of_documents():
	for doc in TYPES_OF_DOCUMENTS:
		if not frappe.db.exists("Type of document", doc["document_type"]):
			dt = frappe.get_doc({
				"doctype": "Type of document",
				"type_of_document": doc["document_type"],
				"description_en": doc["definition_en"],
				"description_fr": doc["definition_fr"]
			})
			dt.insert(ignore_permissions=True)

		for example in doc["examples"]:
			frappe.get_doc({
				"doctype": "Type of document example",
				"example": example,
				"type_of_document": doc["document_type"]
			}).insert(ignore_permissions=True)

def create_rejection_reasons():
	for rejection_reason in REJECTION_REASONS:
		doc = frappe.get_doc({"doctype": "Supplier Rejection Reason"})
		doc.update(rejection_reason)
		doc.insert(ignore_permissions=True)

def create_legalization_questions():
	for question in QUESTIONS:
		doc = frappe.get_doc({"doctype": "Legalization Question"})
		doc.update(question)
		doc.insert(ignore_permissions=True)

def create_exception_cases():
	for case in EXCEPTIONS:
		doc = frappe.get_doc({"doctype": "Legalization Exception"})
		doc.update(case)
		doc.insert(ignore_permissions=True)

def create_legalization_per_country():
	with open(os.path.join(os.path.dirname(__file__), "legalization_per_country.json"), "r") as f:
		entries = json.loads(f.read())

	for entry in entries:
		doc = frappe.get_doc({"doctype": "Legalization Rule"})
		doc.update(entry)
		doc.insert(ignore_permissions=True)

def after_sync():
	try:
		initialize_system()
		add_countries()
		create_french_address_template()
		create_types_of_documents()
		create_rejection_reasons()
		create_legalization_questions()
		create_exception_cases()
		create_legalization_per_country()
		create_consulates()
		execute()
		frappe.db.set_value("System Settings", "System Settings", "app_name", "Popcorn")
	except Exception:
		print(frappe.get_traceback())