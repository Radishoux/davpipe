# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "popcorn"
app_title = "Popcorn"
app_publisher = "Davron Digital"
app_description = "Legalization Application"
app_icon = "fa fa-file-contract"
app_color = "#013781"
app_email = "hello@davrondigital.com"
app_license = "Proprietary"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
app_include_css = "/assets/css/popcorn.css"
app_include_js = "/assets/js/popcorn.js"

app_logo_url = '/assets/popcorn/images/PopcornFavicon30_500x500.png'

website_context = {
	"favicon": 	"/assets/popcorn/images/favicon.ico",
	"splash_image": "/assets/popcorn/images/PopcornLogo_585x103.png"
}

# include js, css files in header of web template
web_include_css = "/assets/css/website-custom.css"
# web_include_js = "/assets/js/popcorn.js"

# include custom scss in every website theme (without file extension ".scss")
# website_theme_scss = "popcorn/public/scss/website"

# include js, css files in header of web form
# webform_include_js = {"doctype": "public/js/doctype.js"}
# webform_include_css = {"doctype": "public/css/doctype.css"}

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
# doctype_js = {"doctype" : "public/js/doctype.js"}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

get_translated_dict = {
	("boot", None): "popcorn.utils.translations.get_translations"
}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# get_website_user_home_page = "/app/project"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "popcorn.install.before_install"
after_sync = "popcorn.install.install.after_sync"

before_migrate = "popcorn.install.before_migrate.execute"
after_migrate = "popcorn.install.after_migrate.execute"

boot_session = "popcorn.startup.boot_session"

on_login = "popcorn.startup.on_login"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "popcorn.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# DocType Class
# ---------------
# Override standard doctype classes

# override_doctype_class = {
# 	"ToDo": "custom_app.overrides.CustomToDo"
# }

# Document Events
# ---------------
# Hook on document methods and events

standard_queries = {
	"Country": "popcorn.utils.queries.get_countries"
}

doc_events = {
	"File": {
		"before_insert": "popcorn.utils.file.before_file_insert",
		"after_insert": "popcorn.utils.file.after_file_insert",
		"on_trash": "popcorn.utils.file.on_file_trash"
	}
}

# Scheduled Tasks
# ---------------

scheduler_events = {
	"all": [
		"popcorn.popcorn.doctype.tezos_digisign_job.tezos_digisign_job.check_digisign_job_status",
	],
	"hourly": [
		"popcorn.popcorn.doctype.tezos_digisign_job.tezos_digisign_job.get_digisign_proof",
	],
	"cron": {
		"0 23 * * *": [
			"popcorn.utils.audit.create_audit_log_files"
		]
	},
	"daily": [
		"popcorn.popcorn.doctype.task.task.check_late_tasks",
		"popcorn.popcorn.doctype.project.project.delete_old_drafts",
		"popcorn.utils.tezos.send_to_tezos"
	]
	# "weekly": [
	# 	"popcorn.tasks.weekly"
	# ]
	# "monthly": [
	# 	"popcorn.tasks.monthly"
	# ]
}

# Testing
# -------

# before_tests = "popcorn.install.before_tests"

# Overriding Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "popcorn.event.get_events"
# }
#
# each overriding function accepts a `data` argument;
# generated from the base implementation of the doctype dashboard,
# along with any modifications made in other Frappe apps
# override_doctype_dashboards = {
# 	"Task": "popcorn.task.get_dashboard_data"
# }

# exempt linked doctypes from being automatically cancelled
#
# auto_cancel_exempted_doctypes = ["Auto Repeat"]

