import json
import frappe

@frappe.whitelist(allow_guest=True)
def tezos_callback(*args, **kwargs):
	frappe.log_error(args, "Tezos Callback Succeeded")