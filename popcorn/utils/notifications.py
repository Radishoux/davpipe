import frappe
from frappe import _
from frappe.utils import format_date, cint, now_datetime, format_datetime, add_days, date_diff, today, getdate,list_to_str
from frappe.contacts.doctype.address.address import get_preferred_address, get_address_display

class PopcornNotification:
	def __init__(self, template, project, task=None):
		self.data = {}
		self.project = project
		self.project_doc = frappe.get_doc("Project", self.project)
		self.task = task
		self.project_name = self.project_doc.project_name
		self.project_url = frappe.utils.get_link_to_form("Project", self.project, _("here"))
		self.customer_name = self.project_doc.customer
		self.template = template
		self.subject = _("Popcorn Notification")
		self.recipients = self.get_recipients()

	def send_notification(self):
		message = frappe.render_template(f"templates/emails/{self.template}.html", self.data)
		com_kwargs = {
			"recipients": self.recipients,
			"subject": self.subject,
			"message": message,
			"doc": self.project_doc
		}

		try:
			frappe.sendmail(
				recipients = self.recipients,
				subject = self.subject,
				template = self.template,
				args = self.data,
				reference_doctype = "Project",
				reference_name = self.project,
				communication=self.make_communication_entry(**com_kwargs),
			)
		except frappe.exceptions.OutgoingEmailError:
			frappe.log_error(frappe.get_traceback(), "Notification Error")
			frappe.clear_messages()

	def make_communication_entry(self, **kwargs):
		"""Make communication entry"""
		try:
			comm = frappe.get_doc({
				"doctype":"Communication",
				"communication_medium": "Email",
				"sender": kwargs.get("sender"),
				"recipients": list_to_str(kwargs.get("recipients")) if isinstance(kwargs.get("recipients"), list) else kwargs.get("recipients"),
				"subject": kwargs.get("subject"),
				"content": kwargs.get("message"),
				"sent_or_received": "Sent",
				"reference_doctype": kwargs.get("doc", {}).get("doctype"),
				"reference_name": kwargs.get("doc", {}).get("name") if kwargs.get("doc", {}).get("doctype") else None
			})
			comm.insert(ignore_permissions=True)

			return comm.name
		except Exception:
			frappe.log_error(frappe.get_traceback(), _("Notification communication creation error"))
			return

	def get_recipients(self):
		pass

class PopcornSupplierNotifications(PopcornNotification):
	def __init__(self, template, project, task, rejection_reasons=None):
		super(PopcornSupplierNotifications, self).__init__(f"suppliers/{template}", project, task)

		step_number = cint(frappe.db.get_value("Task", self.task, "step_number")) - 1
		self.data = {
			"customer_name": self.customer_name,
			"task_action": _(frappe.db.get_value("Task", self.task, "step")),
			"project_name": self.project_name,
			"project_url": self.project_url,
			"expected_end_date": format_date(frappe.db.get_value("Task", self.task, "expected_end_date")),
			"previous_task": _(frappe.db.get_value("Task", dict(step_number=step_number), "step")),
			"rejection_reasons": rejection_reasons
		}

		self.send_notification()

	def get_recipients(self):
		supplier = frappe.db.get_value("Task", self.task, "supplier")
		return frappe.get_all("Supplier Users",
			filters={"parenttype": "Supplier", "parent": supplier},
			pluck="user",
			distinct=True
		)


class PopcornCustomerNotifications(PopcornNotification):
	def __init__(self, template, project, task=None, rejection_reasons=None, suspension_date=None):
		super(PopcornCustomerNotifications, self).__init__(f"customers/{template}", project, task)

		address = get_preferred_address("Customer", self.customer_name, "is_shipping_address")
		self.data = {
			"customer_name": self.customer_name,
			"supplier_name": frappe.db.get_value("Task", self.task, "supplier") if task else "",
			"project_name": self.project_name,
			"project_url": self.project_url,
			"project": self.project_doc,
			"delivery_address": get_address_display(address) if address else "",
			"rejection_reasons": rejection_reasons,
			"unsuspension_datetime": format_datetime(now_datetime()),
			"new_expected_date": format_date(add_days(self.project_doc.expected_end_date, date_diff(today(), getdate(suspension_date))))
		}

		self.send_notification()

	def get_recipients(self):
		return list({self.project_doc.owner, self.project_doc.modified_by})