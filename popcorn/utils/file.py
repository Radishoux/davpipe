# -*- coding: utf-8 -*-
# Copyright (c) 2020, Davron Digital and contributors
# For license information, please see license.txt

import requests
import os
import io
import base64
import pyqrcode
from PyPDF2 import PdfFileReader, PdfFileWriter

import frappe
from frappe import _
from frappe.utils.pdf import get_pdf
from frappe.core.doctype.navbar_settings.navbar_settings import get_app_logo
from popcorn.utils.audit import create_audit_log, get_content_hash, verify_hash

@frappe.whitelist()
def download_file(filename):
	file_doc = get_file_from_filename(filename)

	frappe.local.response.filename = os.path.basename(file_doc.file_url)
	frappe.local.response.filecontent = file_doc.get_content()
	frappe.local.response.type = "download"

@frappe.whitelist()
def download_certificate(hash):
	file_name, pdf = get_certificate_pdf(hash)

	frappe.local.response.filename = "{name}_certificate.pdf".format(name=file_name.replace(" ", "-").replace("/", "-"))
	frappe.local.response.filecontent = pdf
	frappe.local.response.type = "pdf"

@frappe.whitelist()
def download_file_and_certificate(hash, filename):
	from frappe.utils.pdf import get_file_data_from_writer

	file_doc = get_file_from_filename(filename)
	file_name, certificate = get_certificate_pdf(hash)

	file_doc_pdf = PdfFileReader(io.BytesIO(file_doc.get_content()))
	certificate_pdf = PdfFileReader(io.BytesIO(certificate))
	writer = PdfFileWriter()
	writer.appendPagesFromReader(file_doc_pdf)
	writer.appendPagesFromReader(certificate_pdf)

	frappe.local.response.filename = "{name}_with_certificate.pdf".format(name=file_name.replace(" ", "-").replace("/", "-"))
	frappe.local.response.filecontent = get_file_data_from_writer(writer)
	frappe.local.response.type = "pdf"


def get_certificate_pdf(hash):
	from popcorn.popcorn.page.audit.audit import get_data
	doc = None
	data = verify_hash(hash)
	if data:
		doc = data[0]
		doc["qr_code"], file_name = frappe.db.get_value("File", dict(sha256=hash), ["qr_code", "file_name"])

	if doc:
		html = frappe.render_template("templates/file_certificate.html", {
			"doc": doc,
			"logo": get_app_logo(),
			"audit": get_data(doc.get("project")) if doc.get("project") else ""
		})
		return file_name, get_pdf(html)

def get_file_from_filename(filename, audit=False):
	file_doc = frappe.get_doc("File", filename)
	file_doc.check_permission("read")

	if audit:
		if file_doc.attached_to_doctype == "Project":
			project = file_doc.attached_to_name
		else:
			project = frappe.db.get_value(file_doc.attached_to_doctype, file_doc.attached_to_name, "project")

		create_audit_log("File Download", file_doc.as_dict(), project, frappe.session.user)

	return file_doc

@frappe.whitelist()
def get_files(doctype, docname, file_status):
	status_filter = ("=", file_status)
	if file_status == "Supplied":
		status_filter = ("in", ("Primary", "Secondary"))
	elif file_status == "Additional":
		status_filter = ("in", ("Optional", "ID Card", "Power of Attorney"))

	return frappe.get_all("File", filters={
		"attached_to_doctype": doctype,
		"attached_to_name": docname,
		"file_status": status_filter
	}, fields=["*"])


def before_file_insert(doc, method):
	if not doc.is_folder:
		with open(doc.get_full_path(), 'rb') as f:
			pdf = f.read()

		doc.sha256 = get_content_hash(pdf, "sha256")
		doc.sha512 = get_content_hash(pdf, "sha512")

		if doc.attached_to_doctype == "Task":
			doc.qr_code = generate_qr_code(doc.sha256)

def after_file_insert(doc, method):
	project = None
	if not doc.is_folder:
		if doc.attached_to_doctype == "Project":
			project = doc.attached_to_name
		elif frappe.get_meta(doc.attached_to_doctype, doc.attached_to_name).has_field("project"):
			project = frappe.db.get_value(doc.attached_to_doctype, doc.attached_to_name, "project")

		if project:
			get_file_metadata(doc, project)
			create_audit_log("File Upload", {
				"message": f"File {doc.file_name} has been uploaded",
				"reference_doctype": "File",
				"reference_docname": doc.name
			}, project, frappe.session.user)

def on_file_trash(doc, method):
	if frappe.db.exists("Project", {"original_document": doc.name}):
		frappe.db.set_value("Project", {"original_document": doc.name}, "original_document", None)

def get_file_metadata(doc, project):
	try:
		with open(doc.get_full_path(), 'rb') as f:
			pdf = f.read()


		res = requests.request(
			method="POST",
			url='http://localhost:8080/pdf/',
			files={"file": pdf}
		)

		if res.status_code == 200:
			doc.signature_metadata = res.text
			frappe.db.set_value("File", doc.name, "signature_metadata", res.text)
			create_audit_log("File Metadata", {
				"value": res.text,
				"reference_doctype": "File",
				"reference_docname": doc.name
			}, project, frappe.session.user)

			check_file_integrity(doc)

		# TODO: Error handling when API response is != 200
		# else:
		# 	frappe.throw(_("Your file is not signed. Please upload a signed document"))

	except Exception:
		# TODO: Error handling
		pass
		#frappe.throw(_("Your file could be not be correctly interpreted. Please contact the support."))

#TODO: Check if this needs to be activated
def check_file_integrity(doc):
	meta = frappe.parse_json(doc.signature_metadata)
	if meta:
		last_signature = min(meta, key=lambda x:x.get("signDate"))

		print(doc.sha512)
		print(doc.sha256)
		print(last_signature.get("hash512"))

		# if content_hash != last_signature.get("hash512"):
		# 	frappe.throw(_("The signature content doesn't match your file. Please verify its integrity."))

	#frappe.throw(_("Your file doesn't contain any signature information."))

def generate_qr_code(hash):
	buffer = io.BytesIO()
	url = pyqrcode.create(f"{frappe.utils.get_url()}/verify?hash={hash}")
	url.svg(buffer, scale=4)
	str_equivalent_image = base64.b64encode(buffer.getvalue()).decode()
	return f"<img src='data:image/svg+xml;base64,{str_equivalent_image}'/>"