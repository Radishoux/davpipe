import re
import frappe
from frappe import _
from popcorn.install.countries import get_translated_countries

@frappe.whitelist()
@frappe.validate_and_sanitize_search_inputs
def get_rejection_reasons(doctype, txt, searchfield, start, page_len, filters):
	reasons = frappe.get_all("Supplier Rejection Reason", pluck="name", limit=500)

	output = tuple([d for d in reasons if re.search(txt+".*", _(d), re.IGNORECASE)])

	return [[o] for o in output]

@frappe.whitelist()
@frappe.validate_and_sanitize_search_inputs
def get_countries(doctype, txt, searchfield, start, page_len, filters):
	countries = get_translated_countries(filters)

	output = tuple([d for d in countries if re.search(txt+".*", countries[d], re.IGNORECASE)])

	return [[o] for o in output]