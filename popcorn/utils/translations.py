import frappe
from frappe.translate import load_lang
from popcorn.install.countries import get_translated_countries

def get_translations():
	translations = load_lang(frappe.local.lang, apps=["popcorn"])

	translations.update(get_translated_countries())

	return translations