import hashlib
import os

import frappe
from frappe.utils import nowdate, format_date

def create_audit_log(log_type, value, project, user=None, hash_key=None, audit_file_name=None, audit_file_size=None, sent_to_tezos=None):
	frappe.enqueue("popcorn.utils.audit._create_audit_log",
		log_type=log_type,
		value=value,
		project=project,
		user=user,
		hash_key=hash_key,
		audit_file_name=audit_file_name,
		audit_file_size=audit_file_size,
		sent_to_tezos=sent_to_tezos
	)

def _create_audit_log(log_type, value, project, user=None, hash_key=None, audit_file_name=None, audit_file_size=None, sent_to_tezos=None):
	log = frappe.get_doc({
		"doctype": "Popcorn Audit Log",
		"log_type": log_type,
		"value": frappe.as_json(value) if isinstance(value, dict) or isinstance(value, list) else value,
		"project": project,
		"user": user if user else frappe.session.user,
		"hash_key": hash_key,
		"audit_file_name": audit_file_name,
		"audit_file_size": audit_file_size,
		"sent_to_tezos": sent_to_tezos
	})
	log.insert(ignore_permissions=True)
	frappe.db.commit()

	return log

def get_content_hash(content, algo="sha256"):
	if isinstance(content, str):
		content = content.encode()

	if algo=="sha256":
		return hashlib.sha256(content).hexdigest()
	elif algo=="sha512":
		return hashlib.sha512(content).hexdigest()

@frappe.whitelist(allow_guest=True)
def verify_hash(hash):
	return _verify_hash(hash)

def verify_file(content):
	hash = get_content_hash(content)
	return _verify_hash(hash)

def _verify_hash(hash):
	output = []
	jobs = frappe.get_all("File", filters=dict(sha256=hash, attached_to_doctype="Task"), fields=["sent_to_tezos", "file_name", "attached_to_name"])
	if not jobs:
		jobs = frappe.get_all("Popcorn Audit Log", filters=dict(hash_key=hash), fields=["sent_to_tezos", "audit_file_name", "project"])

	jobs = [frappe._dict(tupleized) for tupleized in set(tuple(item.items()) for item in jobs)]
	for job in jobs:
		project = job.get("project") or frappe.db.get_value("Task", job.get("attached_to_name"), "project")
		project_status, type_of_legalization = frappe.db.get_value("Project", project, ["status", "type_of_legalization"]) if project else (None, None)
		if job.sent_to_tezos:
			tezos_job = frappe.get_doc("Tezos DigiSign Job", job.sent_to_tezos)
			proof = [x for x in frappe.parse_json(tezos_job.digisign_proof) if x.get("hash_document") == hash] if tezos_job.digisign_proof else []
			output.append({
				"hash": hash,
				"file_name": job.get("file_name") or job.get("audit_file_name"),
				"project": project,
				"project_status": project_status,
				"project_type": type_of_legalization,
				"status": tezos_job.status,
				"insertion_datetime": format_date(tezos_job.insertion_datetime) if tezos_job.insertion_datetime else None,
				"injection_datetime": format_date(tezos_job.injection_datetime) if tezos_job.insertion_datetime else None,
				"validation_datetime": format_date(tezos_job.validation_datetime) if tezos_job.insertion_datetime else None,
				"block_hash": tezos_job.block_hash,
				"block_url": f"{frappe.conf.tezos_transaction_url}{tezos_job.block_hash}",
				"transaction_hash": tezos_job.transaction_hash,
				"transaction_url": f"{frappe.conf.tezos_transaction_url}{tezos_job.transaction_hash}",
				"proof": proof[0] if proof else {},
				"signature_verification": True if frappe.conf.tezos_public_key == (proof[0] if proof else {}).get("public_key") else False,
				"merkel_root": frappe.parse_json(frappe.parse_json(tezos_job.get("merkeltree", {})).get("root")).get("hash") if tezos_job.merkeltree else ""
			})
		else:
			output.append({
				"hash": hash,
				"file_name": job.get("file_name") or job.get("audit_file_name"),
				"project": project,
				"project_status": project_status,
				"project_type": type_of_legalization
			})
	else:
		output.append({
			"hash": hash
		})

	return output

def set_project_status(project, status):
	frappe.db.set_value("Project", project, "status", status)
	create_audit_log("Project Modification", {
		"value": status,
		"reference_doctype": "Project",
		"reference_docname": project
	}, project, frappe.session.user)

def set_task_status(task, status):
	frappe.db.set_value("Task", task, "status", status)
	project = frappe.db.get_value("Task", task, "project")
	create_audit_log("Task Modification", {
		"value": status,
		"reference_doctype": "Task",
		"reference_docname": task
	}, project, frappe.session.user)

def set_task_action_status(task_action, status):
	frappe.db.set_value("Task Action", task_action, "status", status)
	project = frappe.db.get_value("Task Action", task_action, "project")
	create_audit_log("Task Action Modification", {
		"value": status,
		"reference_doctype": "Task Action",
		"reference_docname": task_action
	}, project, frappe.session.user)

def create_audit_log_files():
	from collections import defaultdict
	projects = frappe.get_all("Project", filters={f"ifnull(end_date, '{nowdate()}')": (">=", nowdate())}, pluck="name")
	audit_files = frappe.get_all("Popcorn Audit Log",
		filters={"project": ("in", projects), "log_type": ("not in", ("Audit File Creation", "Blockchain Call", "Blockchain Registration"))},
		fields=["name", "log_type", "value", "project", "user", "hash_key", "sent_to_tezos"])

	audit_dict = defaultdict(list)
	for audit_line in audit_files:
		audit_dict[audit_line.project].append(audit_line)

	for project_list in audit_dict:
		if not [x for x in audit_dict[project_list] if not x.get("sent_to_tezos")]:
			continue

		path = frappe.get_site_path("private", "files", "logs", project_list)
		frappe.create_folder(path)

		for audit_entry in audit_dict[project_list]:
			if "value" in audit_entry:
				try:
					audit_entry["value"] = frappe.parse_json(audit_entry["value"])
				except Exception:
					continue

		log_data = frappe.as_json(audit_dict[project_list])

		audit_file_name = f"{format_date(nowdate(), 'YYYY')}{format_date(nowdate(), 'MM')}{format_date(nowdate(), 'DD')}{project_list}.json"
		with open(os.path.join(path, audit_file_name), 'w') as f:
			f.write(log_data)

		with open(os.path.join(path, audit_file_name)) as f:
			create_audit_log(
				log_type="Audit File Creation",
				value=log_data,
				project=project_list,
				user=frappe.session.user,
				hash_key=get_content_hash(f.read().encode()),
				audit_file_name=audit_file_name,
				audit_file_size=os.stat(os.path.join(path, audit_file_name)).st_size
			)

@frappe.whitelist()
def get_audit_logs(project):
	tezos_jobs = list(set(frappe.get_all("Popcorn Audit Log",
		filters={"project": project, "log_type": "Audit File Creation"},
		pluck="sent_to_tezos"
	)))

	transactions = []
	for tezos_job in tezos_jobs:
		transactions.append(
			{
				"transaction_hash": frappe.db.get_value("Tezos DigiSign Job", tezos_job, "transaction_hash"),
				"block_hash": frappe.db.get_value("Tezos DigiSign Job", tezos_job, "block_hash"),
				"status": frappe.db.get_value("Tezos DigiSign Job", tezos_job, "status")
			}
		)

	return transactions