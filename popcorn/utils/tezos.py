import requests
import os
import json

import frappe
from frappe import _
from frappe.utils import format_date, nowdate, now_datetime
from popcorn.utils.audit import create_audit_log

def send_to_tezos(project=None, files=None):
	if not files:
		audit_files = get_audit_files(project)
		uploaded_files = get_uploaded_files(audit_files)
	else:
		audit_files, uploaded_files = get_files_from_job(files, project)

	make_audit_log("Blockchain Call", audit_files + uploaded_files)

	try:
		response = make_request(audit_files + uploaded_files)
	except Exception:
		# TODO: handle exception
		return frappe.log_error(frappe.get_traceback(), "Tezos Error")

	if response.status_code == 200:
		res = response.json()
		job_id = None
		for r in res:
			job = frappe.get_doc({
				"doctype": "Tezos DigiSign Job",
				"id": r.get("jobId"),
				"files": frappe.as_json(r.get("files")),
				"insertion_datetime": now_datetime()
			}).insert(ignore_permissions=True)
			job_id = job.name

		for audit_file in audit_files:
			frappe.db.set_value("Popcorn Audit Log", audit_file.get("name"), "sent_to_tezos", job_id)

			for f in frappe.parse_json(audit_file.get("value", [])):
				if f.get("name"):
					frappe.db.set_value("Popcorn Audit Log", f.get("name"), "sent_to_tezos", job_id)

		for uploaded_file in uploaded_files:
			frappe.db.set_value("File", uploaded_file.get("name"), "sent_to_tezos", job_id)

		make_audit_log("Blockchain Registration", audit_files + uploaded_files, job_id)

		frappe.db.commit()

	else:
		return frappe.log_error(response.json(), "Tezos Request Error")

def get_audit_files(project=None):
	filters = dict(log_type="Audit File Creation", sent_to_tezos=("is", "not set"))

	if project:
		filters.update(dict(project=project))

	return frappe.get_all("Popcorn Audit Log",
		filters=filters,
		fields=["name", "hash_key", "audit_file_name as file_name", "audit_file_size as file_size", "project", "'Popcorn Audit Log' as doctype", "value"])

def get_uploaded_files(audit_files):
	projects = list(set([x.project for x in audit_files]))

	uploaded_files = []
	for project in projects:
		tasks = frappe.get_all("Task", filters=dict(project=project), pluck="name")

		project_files = frappe.get_all("File",
			filters=dict(sent_to_tezos=("is", "not set"), attached_to_doctype="Task", attached_to_name=("in", tasks)),
			fields=["name", "sha256 as hash_key", "file_name", "file_size", f"'{project}' as project", "'File' as doctype"]
		)

		uploaded_files.extend(project_files)

	return uploaded_files

def make_audit_log(log_type, sent_files, tezos_job=None):
	from itertools import groupby

	for project, values in groupby(sent_files, key=lambda x:x['project']):
		create_audit_log(log_type, list(values), project, sent_to_tezos=tezos_job)

def make_request(files):
	site_url = frappe.utils.get_url()
	data = {
		"algorithm": "SHA-256",
		"flowName": f"POPCORN-{format_date(nowdate(), 'YYYY')}{format_date(nowdate(), 'MM')}{format_date(nowdate(), 'DD')}-{now_datetime().strftime('%H:%M')}",
		"callBackUrl": f"{site_url}/api/method/popcorn.webhooks.tezos_callback",
		"files": []
	}

	for audit_file in files:
		data["files"].append(
			{
				"metadata": {
					"fileName": audit_file.get("file_name") or audit_file.get("name") or audit_file.get("fileName"),
					"fileSize": audit_file.get("file_size") or audit_file.get("fileSize"),
					"customFields": {
						"popcorn_document_type": audit_file.get("doctype") or audit_file.get("customFields", {}).get("popcorn_document_type"),
						"popcorn_document_name": audit_file.get("name") or audit_file.get("customFields", {}).get("popcorn_document_name")
					}
				},
				"hash": audit_file.get("hash_key") or audit_file.get("hash")
			}
		)

	return TezosSign().multi_sign(data)

def get_files_from_job(files, project):
	audit_files, uploaded_files = [], []

	for f in files:
		if f.get("customFields", {}).get("popcorn_document_type") == "Popcorn Audit Log":
			f["project"], f["hash_key"] = frappe.db.get_value("Popcorn Audit Log", f.get("customFields", {}).get("popcorn_document_name"), ["project", "hash_key"])
			if f["project"] != project:
				audit_files.append(f)

		if f.get("customFields", {}).get("popcorn_document_type") == "File":
			file_doc = frappe.db.get_value("File", f.get("customFields", {}).get("popcorn_document_name"), ["attached_to_doctype", "attached_to_name", "sha256"])
			f["hash_key"] = file_doc[2]
			if file_doc[0] == "Project":
				f["project"] = file_doc[1]
			elif file_doc[0] == "Task":
				f["project"] = frappe.db.get_value("Task", file_doc[1], "project")
			if f["project"] != project:
				uploaded_files.append(f)

	return audit_files, uploaded_files

class TezosConnector:
	def __init__(self):
		if not frappe.conf.tezos_digisign_token and frappe.conf.tezos_digisign_url:
			frappe.throw(_("Please add a token and an url for Tezos DigiSign in site_config.json"))

		self.token = frappe.conf.tezos_digisign_token
		self.base_url = frappe.conf.tezos_digisign_url
		self.headers = {
			'accept': 'application/json',
			'Content-Type': 'application/json',
			"Authorization": f"Bearer {self.token}"
		}
		self.api_url = ""
		self.data = {}

	def get_request(self):
		return requests.get(
			headers=self.headers,
			url=f'{self.base_url}{self.api_url}'
		)

	def post_request(self):
		return requests.post(
			headers=self.headers,
			url=f'{self.base_url}{self.api_url}',
			data=json.dumps(self.data)
		)

class TezosAccount(TezosConnector):
	def __init__(self):
		super(TezosAccount, self).__init__()

	def get_all_accounts(self):
		self.api_url = "/api/accounts"
		return self.get_request()

	def new_account(self, **kwargs):
		raise NotImplementedError

	def get_account(self, id):
		self.api_url = f"/api/accounts/{id}"
		return self.get_request()

	def patch_account(self, id):
		raise NotImplementedError

	def validate_account(self, id):
		raise NotImplementedError

class TezosAlgorithm(TezosConnector):
	def __init__(self):
		super(TezosAlgorithm, self).__init__()

	def get(self):
		self.api_url = "/api/algorithms"
		return self.get_request()

class TezosSign(TezosConnector):
	def __init__(self):
		super(TezosSign, self).__init__()

	def sign(self, data):
		self.api_url = "/api/sign"
		self.data = data
		return self.post_request()

	def multi_sign(self, data):
		self.api_url = "/api/sign/multi"
		self.data = data
		return self.post_request()

class TezosJobs(TezosConnector):
	def __init__(self):
		super(TezosJobs, self).__init__()

	def get(self, id):
		self.api_url = f"/api/jobs/{id}"
		return self.get_request()

	def get_files(self, id):
		self.api_url = f"/api/jobs/{id}/files"
		return self.get_request()

	def get_merkelTree(self, id):
		self.api_url = f"/api/jobs/{id}/merkelTree"
		return self.get_request()

class TezosFile(TezosConnector):
	def __init__(self):
		super(TezosFile, self).__init__()

	def get_proof(self, id):
		self.api_url = f"/api/files/{id}/proof"
		return self.get_request()