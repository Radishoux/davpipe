import frappe

def execute():
	frappe.reload_doc("popcorn", "doctype", "Type of document example")
	examples = [
		{
			"doctype": "Type of document example",
			"example": "Non marriage certificate",
			"type_of_document": "Administrative deed"
		},
		{
			"doctype": "Type of document example",
			"example": "Rent Receipt",
			"type_of_document": "Private deed (only with certification of signature)"
		},
		{
			"doctype": "Type of document example",
			"example": "Pay Statement",
			"type_of_document": "Private deed (only with certification of signature)"
		},
		{
			"doctype": "Type of document example",
			"example": "Residence Permit",
			"type_of_document": "Administrative deed"
		},
		{
			"doctype": "Type of document example",
			"example": "Driving licence Certificate",
			"type_of_document": "Administrative deed"
		},
		{
			"doctype": "Type of document example",
			"example": "Driving Licence",
			"type_of_document": "Administrative deed"
		},
		{
			"doctype": "Type of document example",
			"example": "Bank Statement",
			"type_of_document": "Private deed (only with certification of signature)"
		},
		{
			"doctype": "Type of document example",
			"example": "Grade Transcript from private school",
			"type_of_document": "Private deed (only with certification of signature)"
		},
		{
			"doctype": "Type of document example",
			"example": "Grade Transcript from public school",
			"type_of_document": "Administrative deed"
		},
		{
			"doctype": "Type of document example",
			"example": "Diploma from private school",
			"type_of_document": "Private deed (only with certification of signature)"
		}
	]

	for example in examples:
		try:
			frappe.get_doc(example).insert(ignore_permissions=True)
		except Exception:
			print(frappe.get_traceback())

	frappe.rename_doc("Type of document example", "Diploma", "Diploma from public school")