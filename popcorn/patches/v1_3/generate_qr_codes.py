import frappe
from popcorn.utils.file import generate_qr_code

def execute():
	frappe.reload_doc("core", "doctype", "file")

	for doc in frappe.get_all("File",
		filters=dict(file_status=("in", ("Primary", "Secondary")), attached_to_doctype="Task"),
		fields=["sha256", "name"]
	):
		frappe.db.set_value("File", doc.name, "qr_code", generate_qr_code(doc.sha256))