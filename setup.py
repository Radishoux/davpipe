# -*- coding: utf-8 -*-
from setuptools import setup, find_packages
import re, ast

_version_re = re.compile(r'__version__\s+=\s+(.*)')

with open('requirements.txt') as f:
	install_requires = f.read().strip().split('\n')

# get version from __version__ variable in popcorn/__init__.py
with open('popcorn/__init__.py', 'rb') as f:
	version = str(ast.literal_eval(_version_re.search(
		f.read().decode('utf-8')).group(1)))

setup(
	name='popcorn',
	version=version,
	description='Legalization Application',
	author='Davron Digital',
	author_email='hello@davrondigital.com',
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
