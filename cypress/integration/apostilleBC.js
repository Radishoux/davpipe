import user from '../fixtures/user.json'

describe('full', () => {
    it('login', () => {
        cy.visit('https://popcorn.davrontranslations.com')

        cy.get('#login_email').type(user.email)
        cy.get('#login_password').type(user.pass)
        cy.get('.btn').contains('Login').click()
    })
    it('Create BCtoapostille project', () => {
        cy.contains('Create a project').click()
        cy.wait(500)
            //   cy.get('input[data-fieldname="project_name"]').type('BCtoapostille') cy.type() can only be called on a single element. Your subject contained 2 elements.Learn more
        cy.get('.control-input > .input-with-feedback').type('BCtoapostille')
        cy.get('input[data-fieldname="country_of_destination"]').type('Australia')
        cy.contains('Next').click()

        // upload yourself
        // cy.get('.align-center > .btn', { timeout: 100000 }).should('be.visible').click()

        // cy.get('.has-error > .form-group > .control-input-wrapper > .control-input > .input-with-feedback').select('Birth certificate')
        // cy.get('.next-btn').click()
        // cy.wait(500)

        // cy.get('div[data-fieldname="send_documents"] > .checkbox > label > .input-area').click() // I want my documents sent once legalized
        // cy.get('.address-box > .btn').then(($btn) => {
        //     if ($btn.innerHTML == "New address") {
        //         cy.get('div[data-fieldname="address_line1"] > .form-group > .control-input-wrapper > .control-input > .input-with-feedback').type('3 Passage petit cerf')
        //         cy.get('div[data-fieldname="address_line2"] > .form-group > .control-input-wrapper > .control-input > .input-with-feedback').type('3 Passage petit cerf')
        //         cy.get('div[data-fieldname="pincode"] > .form-group > .control-input-wrapper > .control-input > .input-with-feedback').type('75017')
        //         cy.get('div[data-fieldname="city"] > .form-group > .control-input-wrapper > .control-input > .input-with-feedback').type('Paris')
        //         cy.get('.modal-body > :nth-child(1) > .form-layout > .form-page > .row > .section-body > .form-column > form > div[data-fieldtype="Link"] > .form-group > .control-input-wrapper > .control-input > .link-field > .awesomplete > .input-with-feedback').type('France')
        //         cy.get('.modal-footer > .standard-actions > .btn-primary').click()
        //     }
        // }) // New address

        // cy.get('div[data-fieldname="translate_doc"] > .checkbox > label > .input-area').click() // My document needs to be translated

        // cy.get('[data-fieldname="type_of_legalization"] > .form-group > .control-input-wrapper > .control-value').click()
        // // ERROR button is disabled unless i focus Type of legalization
        // cy.get('.next-btn').click()

        // // upload yourself
        // cy.get('[data-fieldname="power_of_attorney_documents"] > .file-uploader > .file-preview-area > .file-preview-container > .file-preview > .file-actions > .btn > .icon', { timeout: 100000 }).should('be.visible')
        // cy.get('.next-btn').click()
        // cy.get('.complete-btn', { timeout: 100000 }).should('be.visible').should('not.be.disabled').click()
        // cy.get('#navbar-breadcrumbs > :nth-child(2) > a', { timeout: 100000 }).should('be.visible').click()
    })
})