import user from '../fixtures/user.json'

describe('login', () => {
    it('login', () => {
        cy.visit('https://popcorn.davrontranslations.com')
        cy.get('#login_email').type(user.email)
        cy.get('#login_password').type(user.pass)
        cy.get('.btn').contains('Login').click()
    })
})