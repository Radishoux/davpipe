import user from '../fixtures/user.json'
import 'cypress-file-upload';

describe('translate_Diploma_Austria_France', () => {
    before(function() {
        cy.visit('https://popcorn.davrontranslations.com')
    })

    it('login', () => {
        cy.get('#login_email').type(user.email)
        cy.get('#login_password').type(user.pass)
        cy.get('.btn').contains('Login').click()
    })
    it('enter basic informations', () => {
        cy.contains('Create a project').click()
        cy.get('.control-input > .input-with-feedback', { timeout: 50000 }).type('diploma_to_translate')
        cy.get('input[data-fieldname="country_of_destination"]').type('France{enter}')
            // cy.get('input[data-fieldname="issuing_country"]').type('Austria')
            // cy.contains('Next').click()
    })
    it('upload document', () => {
        const Diploma = 'diploma.pdf'
        cy.get('input[type="file"]').attachFile(Diploma)
        cy.get('.file-name').contains('diploma.pdf')
        cy.get('.align-center > .btn').should('be.visible').click()
    })
    it('informations for documents legalisation', () => {
        cy.get('.has-error > .form-group > .control-input-wrapper > .control-input > .input-with-feedback', { timeout: 50000 }).select('Diploma from public school', { timeout: 50000 })
        cy.get('select[data-fieldname="legalization_question_1"]').select('Yes')
        cy.get('.next-btn').click()
        cy.get('div[data-fieldname="send_documents"] > .checkbox > label > .input-area', { timeout: 50000 }).click() // I want my documents sent once legalized
        cy.get('.address-box > .btn').then(($btn) => {
                if ($btn.innerHTML == "New address") {
                    cy.get('div[data-fieldname="address_line1"] > .form-group > .control-input-wrapper > .control-input > .input-with-feedback').type(user.address)
                    cy.get('div[data-fieldname="address_line2"] > .form-group > .control-input-wrapper > .control-input > .input-with-feedback').type(user.address)
                    cy.get('div[data-fieldname="pincode"] > .form-group > .control-input-wrapper > .control-input > .input-with-feedback').type(user.dept)
                    cy.get('div[data-fieldname="city"] > .form-group > .control-input-wrapper > .control-input > .input-with-feedback').type(user.city)
                    cy.get('.modal-body > :nth-child(1) > .form-layout > .form-page > .row > .section-body > .form-column > form > div[data-fieldtype="Link"] > .form-group > .control-input-wrapper > .control-input > .link-field > .awesomplete > .input-with-feedback').type('France')
                    cy.get('.modal-footer > .standard-actions > .btn-primary').click()
                }
            }) // New address
        cy.get('div[data-fieldname="translate_doc"] > .checkbox > label > .input-area').click() // My document needs to be translated
            // cy.get('.has-error > .form-group > .control-input-wrapper > .control-input > .input-with-feedback').select('Gefi (CCI)');
        cy.get('.next-btn').click()
    })
    it('extra upload', () => {
        const ID = 'idcard_1.png'
        const POA = 'Power-of-Attorney.jpg'
        const Grades = 'grades.jpg'
        cy.get('[data-fieldname="id_documents"]').find('input[type="file"]').attachFile(ID)
        cy.get('[data-fieldname="power_of_attorney_documents"]').find('input[type="file"]').attachFile(POA)
        cy.get('[data-fieldname="other_proof_documents"]').find('input[type="file"]').attachFile(Grades)
        cy.get('div[data-fieldtype="Small Text"] > .form-group > .control-input-wrapper > .control-input > .input-with-feedback').type('Yes, here is my school report')
        cy.get('.next-btn').click()
    })
    it('verify and go back to home page', () => {
        cy.get('.complete-btn', { timeout: 50000 }).should('be.visible').should('not.be.disabled').click()
        cy.wait(10000);
        cy.get('#navbar-breadcrumbs > :nth-child(2) > a', { timeout: 50000 }).should('be.visible').click()
    })
    it('logout', () => {
        cy.get('.nav-link > .avatar > .avatar-frame').click()
        cy.get('[onclick="return frappe.app.logout()"]').click()
    })
})